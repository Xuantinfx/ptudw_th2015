var express = require('express');
var router = express.Router();
let DanhMucCap2 = require('../controller/DanhMucCap2')
var TrangChuController = require('../controller/TrangChuController_Nhan');
var MatHangController = require('../controller/MatHangController_Nhan');
var localPassport = require('../models/passport')
/* GET home page. */
router.get('/',localPassport.isNotAdminLogin, DanhMucCap2.themDanhMucVaoNavBar, TrangChuController.trangchu); 

//get chi tiết mặt hàng
router.get('/mat-hang/:id', localPassport.isNotAdminLogin, DanhMucCap2.themDanhMucVaoNavBar, MatHangController.getChiTietMatHang);

router.get('/danh-muc/:dm1/:dm2', localPassport.isNotAdminLogin, DanhMucCap2.themDanhMucVaoNavBar, DanhMucCap2.GetDanhMucCap2)

module.exports = router;
