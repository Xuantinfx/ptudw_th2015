const express = require('express');
const router = express.Router();
const nguoiDungController = require('../controller/nguoiDungController')
const DanhMucCap2 = require('../controller/DanhMucCap2')
const localPassport = require('../models/passport')

router.get('/register', DanhMucCap2.themDanhMucVaoNavBar, nguoiDungController.getRegister)
router.post('/register', DanhMucCap2.themDanhMucVaoNavBar, nguoiDungController.postRegister)
router.get('/login', DanhMucCap2.themDanhMucVaoNavBar, nguoiDungController.getLogin)
router.post('/login', DanhMucCap2.themDanhMucVaoNavBar, nguoiDungController.postLogin);
router.post('/logout', nguoiDungController.postLogout)
router.get('/chua-active', DanhMucCap2.themDanhMucVaoNavBar, nguoiDungController.getChuaActive)
router.get('/active', DanhMucCap2.themDanhMucVaoNavBar, nguoiDungController.getActive)
router.get('/quen-mat-khau', DanhMucCap2.themDanhMucVaoNavBar, nguoiDungController.getQuenMatKhau)
router.post('/quen-mat-khau', DanhMucCap2.themDanhMucVaoNavBar, nguoiDungController.postQuenMatKhau);
router.get('/thong-tin-ca-nhan', DanhMucCap2.themDanhMucVaoNavBar,localPassport.isUserLogin , nguoiDungController.getThongTinCaNhan);
router.post('/doi-ten', DanhMucCap2.themDanhMucVaoNavBar, localPassport.isUserLogin, nguoiDungController.postDoiTen)
router.post('/doi-mat-khau', DanhMucCap2.themDanhMucVaoNavBar, localPassport.isUserLogin, nguoiDungController.postDoiMatKhau)

router.get('/gio-hang', DanhMucCap2.themDanhMucVaoNavBar, nguoiDungController.getGioHang);
router.get('/don-hang/tu-choi', localPassport.isUserLogin, DanhMucCap2.themDanhMucVaoNavBar, nguoiDungController.getHuyDonHang);
router.get('/lich-su-mua-hang', localPassport.isUserLogin, DanhMucCap2.themDanhMucVaoNavBar, nguoiDungController.getLichSuMuaHang);
router.post('/gio-hang/cho-sp-vao-gio-hang', DanhMucCap2.themDanhMucVaoNavBar, nguoiDungController.choSanPhamVaoGioHang);
router.post('/gio-hang/dat-hang', localPassport.isUserLogin, nguoiDungController.getChuaActive2, nguoiDungController.datHang);
module.exports = router;