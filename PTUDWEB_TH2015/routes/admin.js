var express = require('express');
var router = express.Router();
const adminNguoiDungControler = require('../controller/adminNguoiDungControler');
const adminQuanTriVienController = require('../controller/adminQuanTriVienController')
const adminSanPhamController = require('../controller/adminSanPhamController');
const localPassport = require('../models/passport')
const adminCaNhanController = require('../controller/adminCaNhanController')
const adminDonHangController = require('../controller/adminDonHangController')
const adminThongKeController = require('../controller/adminThongKeController')
const MatHang = require('../models/MatHang');

/* GET users listing. */
//SẢN PHẨM
router.get('/san-pham',localPassport.isAdminLogin, adminSanPhamController.xemDanhSachSanPham);
router.get('/san-pham/xem-chi-tiet/:idSanPhamXemChiTiet',localPassport.isAdminLogin, adminSanPhamController.xemSanPhamChiTiet);
router.get('/san-pham/xem-truoc-khi-cap-nhat/:idSanPhamCapNhat',localPassport.isAdminLogin, adminSanPhamController.xemTruocKhiCapNhatSanPham);
router.post('/san-pham/cap-nhat/:idSanPhamCapNhat',localPassport.isAdminLogin, adminSanPhamController.capNhatSanPham);
router.post('/san-pham/them/',localPassport.isAdminLogin, adminSanPhamController.themSanPham);
router.get('/san-pham/xoa/:idSanPhamXoa',localPassport.isAdminLogin, adminSanPhamController.xoaSanPham);
//LOAD DANH MỤC CHO <SELECT>, trả về chuõi HTML để gắn
router.get('/san-pham/ds-danh-muc-cap-1',localPassport.isAdminLogin, adminSanPhamController.layDsDanhMucCap1);
router.get('/san-pham/ds-danh-muc-cap-2',localPassport.isAdminLogin, adminSanPhamController.layDsDanhMucCap2);
router.get('/san-pham/ds-thuong-hieu',localPassport.isAdminLogin, adminSanPhamController.layDsThuongHieu);

router.get('/nguoi-dung',localPassport.isAdminLogin, adminNguoiDungControler.getDsNguoiDung);
router.post('/nguoi-dung/them-nguoi-dung',localPassport.isAdminLogin, adminNguoiDungControler.postThemTaiKhoan);
router.post('/nguoi-dung/cap-nhat-nguoi-dung',localPassport.isAdminLogin, adminNguoiDungControler.postCapNhatTaiKhoan);
router.post('/nguoi-dung/reset-password-nguoi-dung',localPassport.isAdminLogin, adminNguoiDungControler.postResetPassWordTaiKhoan);
router.post('/nguoi-dung/xoa-nguoi-dung',localPassport.isAdminLogin, adminNguoiDungControler.postXoaTaiKhoan);

router.get('/admin',localPassport.isAdminLogin, localPassport.isSuperAdminLogin, adminQuanTriVienController.getDsQuanTriVien);
router.post('/admin/them-admin',localPassport.isAdminLogin, localPassport.isSuperAdminLogin, adminQuanTriVienController.postThemAdmin)
router.post('/admin/reset-password-admin',localPassport.isAdminLogin, localPassport.isSuperAdminLogin,adminQuanTriVienController.postResetPassWordAdmin)
router.post('/admin/xoa-admin',localPassport.isAdminLogin, localPassport.isSuperAdminLogin,adminQuanTriVienController.postXoaAdmin)
router.post('/admin/cap-nhat-admin',localPassport.isAdminLogin, localPassport.isSuperAdminLogin,adminQuanTriVienController.postCapNhatAdmin)

router.get("/ca-nhan",localPassport.isAdminLogin, adminCaNhanController.getCaNhan);
router.post('/doi-ten', localPassport.isAdminLogin, adminCaNhanController.postDoiTen)
router.post('/doi-mat-khau', localPassport.isAdminLogin, adminCaNhanController.postDoiMatKhau)

router.get('/don-hang', localPassport.isAdminLogin, adminDonHangController.getDonHang)
router.get('/don-hang/chap-nhan', localPassport.isAdminLogin, adminDonHangController.getChapNhanDonHang)
router.get('/don-hang/tu-choi', localPassport.isAdminLogin, adminDonHangController.getTuChoiDonHang)
router.get('/don-hang/huy-don', localPassport.isAdminLogin, adminDonHangController.getHuyGiaoHang)
router.get('/don-hang/thanh-cong', localPassport.isAdminLogin, adminDonHangController.getGiaoHangThanhCong)

router.get('/thong-ke', localPassport.isAdminLogin, adminThongKeController.getThongKe)

var multer = require('multer');
var fs = require('fs');

var storage = multer.diskStorage({
    destination:function(req,file,cb){
        cb(null,'./public/assets/images');
    },
    filename: function(req,file,cb){
        cb(null,req.query.maSo+'.jpg');
    }
 })

var upload = multer({storage:storage});

router.post('/up-img', localPassport.isAdminLogin, upload.single('file'), (req, res, next) => {
  //lấy {maso, ten, giá, khối lượng tịnh, số lượng sp con}
    MatHang.find({}, 'maSo ten gia khoiLuongTinh soLuongSPTrongMatHang', (err, data) => {
        if (err) throw err;
        res.render('admin/sanpham', {
            layout: 'admin/layout',
            dsSanPham: data,
            user: req.user
        });
    })
});

router.get('/dashboard',localPassport.isAdminLogin, function(req, res, next) {
  return res.render('admin/dashboard', {layout: 'admin/layout', user: req.user, dashboard: true});
});

module.exports = router;
