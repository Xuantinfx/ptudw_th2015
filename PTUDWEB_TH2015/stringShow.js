module.exports = {
    emptyEmail: "Email không được rỗng",
    exitsEmail: "Email đã tồn tại, vui lòng thử email khác",
    emptyHoVaTen: "Họ và tên không được rỗng",
    emptyPassword: "Password không được rỗng",
    emptyRePassword: "Nhập lại mật khẩu không được rỗng",
    notMatchPassword: "Mật khẩu và mật khẩu nhập lại không khớp",
    registerSuccessAndLogin: "Đăng ký thành công, đăng nhập để trãi nghiệm",
    wrongEmail: "Email không tồn tại",
    wrongPass: "Mật khẩu không đúng",
    activateAccount: "Kích hoạt tài khoản"
}