var mongoose = require("mongoose");

//define Schema
var Schema = mongoose.Schema;

// var SchemaDacTrung = new Schema({
//     ten: { //tên của loại đặc trưng sau đó là giá trị, phân cách bởi dấu 2 chấm. vd: "Loại nước:Nước tinh khiết"
//         type: String,
//         required: true
//     },
//     idMatHang: { //id những mặt hàng có đặc trưng ấy
//         type: [Schema.Types.ObjectId]
//     }
// })

var SchemaThuongHieu = new Schema({
    //Mã số dùng để lấy hình ảnh tương ứng
    maSo: {
        type: String,
        required: true
    },
    ten: {
        type: String,
        required: true
    },
    // dacTrung: {
    //     type: [SchemaDacTrung]
    // }
    idMatHang: { //id những mặt hàng có đặc trưng ấy
        type: [{type: Schema.Types.ObjectId, ref: 'MatHang'}]
    }
})

var SchemaDanhMucCap2 = new Schema({
    //Mã số dùng để lấy hình ảnh tương ứng
    maSo: {
        type: String,
        required: true
    },
    ten: {
        type: String,
        required: true
    },
    thuongHieu: {
        type: [SchemaThuongHieu]
    }
})

var SchemaDanhMucCap1 = new Schema({
    //Mã số dùng để lấy hình ảnh tương ứng
    maSo: {
        type: String,
        required: true
    },
    ten: {
        type: String,
        required: true
    },
    danhMucCap2: {
        type: [SchemaDanhMucCap2]
    }
})

var DanhMucCap1 = module.exports = mongoose.model('DanhMucCap1', SchemaDanhMucCap1);