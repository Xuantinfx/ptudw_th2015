const mongoose = require('mongoose');

const Schema = mongoose.Schema;

const matHangMua = new Schema({
    idMatHang: { type: Schema.Types.ObjectId, required: true, ref: 'MatHang'},
    soLuong: { type: Number, min: 1, required: true },
    gia: { type: Number, min: 0, required: true },
    thanhTien: { type: Number, min: 0, required: true }
})

const DonHang = new Schema({
    nguoiMua: { type: Schema.Types.ObjectId, required: true, ref: 'NguoiDung'},
    ngayLap: { type: Date, required: true, default: new Date() },
    diaChi: { type: String, required: true },
    danhSachMua: [ matHangMua ],
    tongTien: { type: Number, required: true },
    trangThai: { type: String, default: 'Chưa xử lí' },
    moTaTrangThai: { type: String, default: '' },
    ngayGiao: { type: Date }
})

module.exports = mongoose.model('DonHang',DonHang);