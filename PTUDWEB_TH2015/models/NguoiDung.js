var mongoose = require('mongoose');

var Schema = mongoose.Schema;

var SchemaSanPhamTrongGio = new Schema({
    idMatHang: {
        type: Schema.Types.ObjectId,
        ref: 'MatHang'
    },
    maSo: {
        type: String
    },
    soLuong: {
        type: Number,
        min: 0
    }
})

//dùng cho lịch sử đơn hàng, chứa id của đơn hàng
var SchemaDonHang = new Schema({
    idDonHang: {
        type: Schema.Types.ObjectId,
        required: true,
        ref: "DonHang"
    }
})

var SchemaNguoiDung = new Schema({
    email: {
        type: String,
        required: true
    },
    matKhau: {
        type: String,
        required: true
    },
    hoVaTen: {
        type: String,
        required: true
    },
    gioHang: {
        type: [SchemaSanPhamTrongGio]
    },
    lichSuMuaHang: {
        type: [SchemaDonHang]
    }, 
    activationCode: {
        type: String,
        required: true
    }

})

var NguoiDung = module.exports = mongoose.model('NguoiDung', SchemaNguoiDung);