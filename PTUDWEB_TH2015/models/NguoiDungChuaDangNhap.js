var mongoose = require('mongoose');

var Schema = mongoose.Schema;

var SchemaSanPhamTrongGio = new Schema({
    idMatHang: {
        type: Schema.Types.ObjectId,
        ref: 'MatHang'
    },
    maSo : {
        type: String
    },
    soLuong: {
        type: Number,
        min: 0
    }
})

var SchemaNguoiDungChuaDangNhap = new Schema({
    sessionID:{
        type: String,
    },
    gioHang: {
        type: [SchemaSanPhamTrongGio]
    }
})

var NguoiDungChuaDangNhap = module.exports = mongoose.model('NguoiDungChuaDangNhap', SchemaNguoiDungChuaDangNhap);