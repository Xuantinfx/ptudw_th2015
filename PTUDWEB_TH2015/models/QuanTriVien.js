var mongoose = require('mongoose');

var Schema = mongoose.Schema;

var SchemaQuanTriVien = new Schema({
    email: {
        type: String,
        required: true
    },
    matKhau: {
        type: String,
        required: true
    },
    hoVaTen: {
        type: String,
        required: true
    },
    super: {
        type: String, 
        required: true
    }
})

var QuanTriVien = module.exports = mongoose.model('QuanTriVien', SchemaQuanTriVien);