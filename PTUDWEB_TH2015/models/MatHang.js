const mongoose = require('mongoose');

const Schema = mongoose.Schema;

const MatHangHayMuaChung = new Schema({
    idMatHang: { type: Schema.Types.ObjectId, required: true, ref: 'MatHang'},
    soLanMua: { type: Number, default: 0 }
})

const BinhLuan = new Schema({
    ten: { type: String, required: true },
    binhLuan: { type: String, required: true }
})

const MatHang = new Schema({
    maSo: {type: String, required: true},
    ten: { type: String, required: true },
    soLuongSPTrongMatHang: { type: Number , default: 0, min: 0 },
    khoiLuongTinh: { type: String },
    gia: { type: Number, required: true, min: 0 },
    tinhTrang: { type: String, default: 'Còn hàng' },
    thongTinChiTiet: { thanhPhan: String , huongDanSuDung: String, baoQuan: String },
    moTa: { type: String, default: ''},
    danhSachMatHangHayMuaChung: [ MatHangHayMuaChung ],
    danhSachBinhLuan: [ BinhLuan ]
})

module.exports = mongoose.model('MatHang', MatHang)