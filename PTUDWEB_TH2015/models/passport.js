var LocalStrategy = require('passport-local').Strategy;
var NguoiDung = require('./NguoiDung');
var QuanTriVien = require('./QuanTriVien')
var bcrypt = require('bcryptjs')
var stringShow = require('../stringShow')
var passport = require('passport');

passport.use(new LocalStrategy({
    passReqToCallback: true
}, (req, email, pass, done) => {
    NguoiDung.findOne({
        email: email
    }, (err, user) => {
        if (err) return done(err);
        // la nguoi dung
        if (user) {
            //so sanh pass
            bcrypt.compare(pass, user.matKhau, function (err, isMatch) {
                if (err) return done(err);
                if (isMatch) {
                    if (req.userChuaDangNhap.gioHang.length != 0) { //nếu người dùng có hàng trong giỏ lúc chưa đăng nhập
                        user.gioHang = req.userChuaDangNhap.gioHang;
                        user.save((err) => {
                            if (err) console.log(err);
                            return done(null, {
                                type: 'user',
                                user: user
                            })
                        })
                    } else return done(null, {
                        type: 'user',
                        user: user
                    });
                } else {
                    req.flash("username", email)
                    req.flash("password", pass)
                    return done(null, false, req.flash("error", stringShow.wrongPass));
                }
            });
        }
        if (!user) {
            //co the o day la admin
            QuanTriVien.findOne({
                email: email
            }, (err, admin) => {
                if (err) return done(err)
                if (admin) {
                    //so sanh pass
                    bcrypt.compare(pass, admin.matKhau, function (err, isMatch) {
                        if (err) return done(err);
                        if (isMatch) {
                            return done(null, {
                                type: 'admin',
                                user: admin
                            });
                        } else {
                            req.flash("username", email)
                            req.flash("password", pass)
                            return done(null, false, req.flash("error", stringShow.wrongPass));
                        }
                    });
                }
                if (!admin) {
                    req.flash("username", email)
                    req.flash("password", pass)
                    return done(null, false, req.flash("error", stringShow.wrongEmail));
                }
            })
        }
    })
}))

passport.serializeUser((user, done) => {
    done(null, {
        type: user.type,
        id: user.user.id
    })
})

passport.deserializeUser((id, done) => {
    //nguoi dung
    if (id.type == 'user') {
        NguoiDung.findById(id.id, (err, result) => {
            done(err, { ...result.toObject(),
                type: 'user'
            });
        })
    } else {
        QuanTriVien.findById(id.id, (err, result) => {
            done(err, { ...result.toObject(),
                type: 'admin'
            });
        })
    }
})

module.exports.isAdminLogin = (req, res, next) => {
    if (req.isUnauthenticated()) {
        return res.redirect('/');
    }
    if (req.user.type == 'admin')
        next();
    else {
        res.redirect('/')
    }
}

module.exports.isSuperAdminLogin = (req, res, next) => {
    if (req.user.super == 'true')
        next();
    else {
        res.redirect('/admin/dashboard')
    }
}

module.exports.isNotAdminLogin = (req, res, next) => {
    if (req.user) {
        if (req.user.type == 'admin')
            res.redirect('/admin/dashboard')
        else next();
    } else {
        next()
    }
}

module.exports.isUserLogin = (req, res, next) => {
    if (req.user) {
        if (req.user.type == 'user') {
            next();
        } else {
            res.redirect('/')
        }
    } else {
        res.redirect('/')
    }
}