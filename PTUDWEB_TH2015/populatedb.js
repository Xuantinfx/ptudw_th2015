#! /usr/bin/env node


// Lấy connection string của database
var userArgs = process.argv.slice(2);
if (!userArgs[0].startsWith('mongodb')) {
    console.log('ERROR: You need to specify a valid mongodb URL as the first argument');
    return
}

var async = require('async')
var DanhMucCap1 = require('./models/DanhMucCap1.js')
var DonHang = require('./models/DonHang.js')
var MatHang = require('./models/MatHang.js')
var NguoiDung = require('./models/NguoiDung.js')
var NguoiDungChuaDangNhap = require('./models/NguoiDungChuaDangNhap.js')
var QuanTriVien = require('./models/QuanTriVien.js')



var mongoose = require('mongoose');
var mongoDB = userArgs[0];
mongoose.connect(mongoDB);
mongoose.Promise = global.Promise;
var db = mongoose.connection;
mongoose.connection.on('error', console.error.bind(console, 'MongoDB connection error:'));

//mảng lưu lại các đối tượng đã được tạo ra
var dsDanhMucCap1 = []
var dsDonHang = []
var dsMatHang = []
var dsNguoiDung = []
var dsNguoiDungChuaDangNhap = []
var dsQuanTriVien = []


//định nghĩa hàm tạo 1 document (1)
function taoQuanTriVien(email, matKhau, hoVaTen, superUser, callback) {
    var dummy = {
        email: email,
        matKhau: matKhau,
        hoVaTen: hoVaTen,
        super: superUser
    }

    var qtv = new QuanTriVien(dummy);

    qtv.save((err, result) => {
        if (err) {
            console.log(err);
            callback(err, null);
            return;
        }
        console.log('Them thanh cong QTV moi: ', result._id);
        dsQuanTriVien.push(qtv);
        callback(null, qtv);
    })
}

function taoMatHang(maSo, ten, soLuongSPTrongMatHang, khoiLuongTinh, gia, tinhTrang, thongTinChiTiet, moTa, matHangHayMuaChung, danhSachBinhLuan, callback) {
    var dummy = {
        maSo: maSo,
        ten: ten,
        soLuongSPTrongMatHang: soLuongSPTrongMatHang,
        khoiLuongTinh: khoiLuongTinh,
        gia: gia,
        tinhTrang: tinhTrang,
        thongTinChiTiet: thongTinChiTiet,
        moTa: moTa,
        matHangHayMuaChung: matHangHayMuaChung,
        danhSachBinhLuan: danhSachBinhLuan
    }

    var mathang = new MatHang(dummy);

    mathang.save((err, result) => {
        if (err) {
            console.log(err);
            callback(err, null);
            return;
        }
        console.log('Them thanh cong mat hang moi: ', result._id);
        dsMatHang.push(mathang);
        callback(null, mathang);
    })
}

function taoNguoiDung(email, matKhau, hoVaTen, gioHang, lichSuMuaHang, activationCode, callback) {
    var dummy = {
        email: email,
        matKhau: matKhau,
        hoVaTen: hoVaTen,
        gioHang: gioHang,
        lichSuMuaHang: lichSuMuaHang,
        activationCode: activationCode
    }

    var nguoidung = new NguoiDung(dummy);

    nguoidung.save((err, result) => {
        if (err) {
            console.log(err);
            callback(err, null);
            return;
        }
        console.log('Them thanh cong nguoi dung moi: ', result._id);
        dsNguoiDung.push(nguoidung);
        callback(null, nguoidung);
    })
}

function taoDonHang(nguoiMua, ngayLap, diaChi, danhSachMua, tongTien, trangThai, moTaTrangThai, ngayGiao, callback) {
    var dummy = {
        nguoiMua: nguoiMua,
        ngayLap: ngayLap,
        diaChi: diaChi,
        danhSachMua: danhSachMua,
        tongTien: tongTien,
        trangThai: trangThai,
        moTaTrangThai: moTaTrangThai,
        ngayGiao: ngayGiao
    }

    var donhang = new DonHang(dummy);

    donhang.save((err, result) => {
        if (err) {
            console.log(err);
            callback(err, null);
            return;
        }
        console.log('Them thanh cong don hang moi: ', result._id);
        dsDonHang.push(donhang);
        callback(null, donhang)
    })
}

function taoDanhMucCap1(maSo, ten, danhMucCap2, callback) {
    var dummy = {
        maSo: maSo,
        ten: ten,
        danhMucCap2: danhMucCap2,
    }

    var danhmuccap1 = new DanhMucCap1(dummy);

    danhmuccap1.save((err, result) => {
        if (err) {
            console.log(err);
            callback(err, null);
            return;
        }
        console.log('Them thanh cong danh muc cap 1 moi: ', result._id);
        dsDanhMucCap1.push(danhmuccap1);
        callback(null, danhmuccap1)
    })
}


//định nghĩa các hàm tạo dữ liệu mẫu (2)
function taoDuLieuMau1(cb) {
    async.series([
        function (callback) {
            taoQuanTriVien('qtv@hotmail.com', '123456', 'Nhân Nguyễn', "false", callback);
        },
        function (callback) {
            taoQuanTriVien('abc@gmail.com', '123456', 'Nguyễn Nam', "false", callback);
        },
        function (callback) {
            taoQuanTriVien('lalaland@gmail.com', '123456', 'Chú Bảy', "true", callback);
        },
        function (callback) {
            taoNguoiDung('ddnn97@hotmail.com', '1', 'Nhân', [], [], '0', callback);
        },
        function (callback) {
            taoNguoiDung('nguoidung2@gmail.com', '123456', 'Người dùng 2', [], [], '0', callback);
        },
        function (callback) {
            taoNguoiDung('nguoidung3@gmail.com', '123456', 'Người dùng 3', [], [], '0', callback);
        },
        //nước tinh khiết
        function (callback) {
            taoMatHang('sp-nuoc-tinh-khiet-aquafina',
                'Nước tinh khiết Aquafina ',
                1, '500ml', 5000, 'Còn hàng', {
                    thanhPhan: 'Nước tinh khiết Aquafina cung cấp nguồn nước hoàn toàn sạch, bổ sung lại lượng nước mà bạn bị mất do các vận động trong cả ngày dài.',
                    huongDanSuDung: 'Dùng trực tiếp, ngon hơn khi uống lạnh.',
                    baoQuan: 'Để ở nơi khô ráo thoáng mát, tránh áng nắng mặt trời.'
                },
                'Hương vị nước tinh khiết. Nguồn nước ngầm chất lượng cao. Dùng để giải khát, bổ sung nước', [], [], callback);
        },
        function (callback) {
            taoMatHang('sp-nuoc-tinh-khiet-lavie',
                'Nước tinh khiết Lavie',
                1, '500ml', 5000, 'Còn hàng', {
                    thanhPhan: 'Nước khoáng Lavie được khai thác ngay tại mạch nước khoáng ngầm sâu trong lòng đất và được chắt lọc qua nhiều tầng địa chất nên giữ nguyên được hương vị thiên nhiên tươi mát.',
                    huongDanSuDung: 'Dùng trực tiếp, ngon hơn khi uống lạnh.',
                    baoQuan: 'Để ở nơi khô ráo thoáng mát, tránh áng nắng mặt trời.'
                },
                'Hương vị nước tinh khiết. Chứa nhiều khoáng chất, hàm lượng thấp. Dùng để giải khát, bổ sung nước', [], [], callback);
        },
        function (callback) {
            taoMatHang('sp-nuoc-tinh-khiet-vinh-hao',
                'Nước tinh khiết Vĩnh Hảo',
                1, '500ml', 5000, 'Còn hàng', {
                    thanhPhan: 'Nguồn nước được tinh lọc bằng hệ thống Siêu lọc RO và tiệt trùng hoàn toàn bằng tia cực tím UV, do đó loại bỏ được vi khuẩn và những tạp chất độc hại',
                    huongDanSuDung: 'Dùng trực tiếp, ngon hơn khi uống lạnh.',
                    baoQuan: 'Để ở nơi khô ráo thoáng mát, tránh áng nắng mặt trời.'
                },
                'Vị chanh thơm nhẹ, ngọt dịu. Chứa khoáng chất vi lượng. Dùng để giải khát, bổ sung nước', [], [], callback);
        },
        function (callback) {
            taoMatHang('sp-nuoc-tinh-khiet-dasani',
                'Nước tinh khiết Dasani',
                1, '500ml', 5000, 'Còn hàng', {
                    thanhPhan: 'Nước tinh khiết Dasani là sản phẩm sử dụng nguồn nước tinh khiết đóng chai với chất lượng cao.',
                    huongDanSuDung: 'Dùng trực tiếp, ngon hơn khi uống lạnh.',
                    baoQuan: 'Để ở nơi khô ráo thoáng mát, tránh áng nắng mặt trời.'
                },
                'Hương vị nước tự nhiên. Sử dụng nguồn nước tinh khiết. Giải khát, bù nước nhanh chóng', [], [], callback);
        },
        //bia
        function (callback) {
            taoMatHang('sp-bia-333',
                'Bia 333 lon',
                1, '330ml', 10000, 'Còn hàng', {
                    thanhPhan: 'Nước, đại mạch, ngũ cốc, hoa bia,...',
                    huongDanSuDung: 'Ngon hơn khi uống lạnh, tốt nhất ở 10 – 15 độ C.',
                    baoQuan: 'Để ở nơi khô ráo thoáng mát, tránh áng nắng mặt trời.'
                },
                'Nồng độ cồn 5.3%. Hương lúa đại mạch thơm ngon. Ngon hơn khi uống lạnh (10-15 độ)', [], [], callback);
        },
        function (callback) {
            taoMatHang('sp-bia-larue-xanh-duong',
                'Bia Larue xanh dương',
                1, '330ml', 10000, 'Còn hàng', {
                    thanhPhan: 'Nước, đại mạch, ngũ cốc, hoa bia,...',
                    huongDanSuDung: 'Ngon hơn khi uống lạnh, tốt nhất ở 10 – 15 độ C.',
                    baoQuan: 'Để ở nơi khô ráo thoáng mát, tránh áng nắng mặt trời.'
                },
                'Nồng độ cồn 5.3%. Hương lúa đại mạch thơm ngon. Ngon hơn khi uống lạnh (10-15 độ)', [], [], callback);
        },
        function (callback) {
            taoMatHang('sp-bia-sai-gon-lager',
                'Bia Sài Gòn Lager',
                1, '330ml', 10000, 'Còn hàng', {
                    thanhPhan: 'Nước, đại mạch, ngũ cốc, hoa bia,...',
                    huongDanSuDung: 'Ngon hơn khi uống lạnh, tốt nhất ở 10 – 15 độ C.',
                    baoQuan: 'Để ở nơi khô ráo thoáng mát, tránh áng nắng mặt trời.'
                },
                'Nồng độ cồn 5.3%. Hương lúa đại mạch thơm ngon. Ngon hơn khi uống lạnh (10-15 độ)', [], [], callback);
        },
        function (callback) {
            taoMatHang('sp-bia-su-tu-trang-premium-lager',
                'Bia Sư Tử Trắng Premium Lager',
                1, '330ml', 10000, 'Còn hàng', {
                    thanhPhan: 'Nước, đại mạch, ngũ cốc, hoa bia,...',
                    huongDanSuDung: 'Ngon hơn khi uống lạnh, tốt nhất ở 10 – 15 độ C.',
                    baoQuan: 'Để ở nơi khô ráo thoáng mát, tránh áng nắng mặt trời.'
                },
                'Nồng độ cồn 5.3%. Hương lúa đại mạch thơm ngon. Ngon hơn khi uống lạnh (10-15 độ)', [], [], callback);
        },
        //sữa tươi
        function (callback) {
            taoMatHang('sp-sua-tiet-trung-dutch-lady-dau',
                'Sữa tiệt trùng Dutch Lady Dâu',
                1, '220ml', 5000, 'Còn hàng', {
                    thanhPhan: 'Nước, sữa bột, đường tinh luyện, dầu thực vật, vitamin A, vitamin D3...',
                    huongDanSuDung: 'Lắc đều trước khi uống. Ngon hơn khi uống lạnh.',
                    baoQuan: 'Để ở nơi khô ráo thoáng mát, tránh áng nắng mặt trời.'
                },
                'Hương vị béo ngọt, thơm ngon. Sữa tiệt trùng có đường. Bổ sung chất béo thực vật, vitamin A,D', [], [], callback);
        },
        function (callback) {
            taoMatHang('sp-sua-tiet-trung-nutifood',
                'Sữa tiệt trùng Nutifood',
                1, '220ml', 5000, 'Còn hàng', {
                    thanhPhan: 'Nước, sữa bột, đường tinh luyện, dầu thực vật, vitamin A, vitamin D3...',
                    huongDanSuDung: 'Lắc đều trước khi uống. Ngon hơn khi uống lạnh.',
                    baoQuan: 'Để ở nơi khô ráo thoáng mát, tránh áng nắng mặt trời.'
                },
                'Hương vị béo ngọt, thơm ngon. Sữa tiệt trùng có đường. Bổ sung chất béo thực vật, vitamin A,D', [], [], callback);
        },
        function (callback) {
            taoMatHang('sp-sua-tiet-trung-vinamilk-cd-star-fino',
                'Sữa tiệt trùng Vinamilk Star có đường',
                1, '220ml', 5000, 'Còn hàng', {
                    thanhPhan: ' Nước, sữa bột, đường tinh luyện, dầu thực vật, vitamin A, vitamin D3...',
                    huongDanSuDung: 'Lắc đều trước khi uống. Ngon hơn khi uống lạnh.',
                    baoQuan: 'Để ở nơi khô ráo thoáng mát, tránh áng nắng mặt trời.'
                },
                'Hương vị béo ngọt, thơm ngon. Sữa tiệt trùng có đường. Bổ sung chất béo thực vật, vitamin A,D', [], [], callback);
        },
        function (callback) {
            taoMatHang('sp-sua-tiet-trung-vinamilk-socola',
                'Sữa tiệt trùng Vinamilk Socola',
                1, '220ml', 5000, 'Còn hàng', {
                    thanhPhan: ' Nước, sữa bột, đường tinh luyện, dầu thực vật, vitamin A, vitamin D3...',
                    huongDanSuDung: 'Lắc đều trước khi uống. Ngon hơn khi uống lạnh.',
                    baoQuan: 'Để ở nơi khô ráo thoáng mát, tránh áng nắng mặt trời.'
                },
                'Hương vị béo ngọt, thơm ngon. Sữa tiệt trùng có đường. Bổ sung chất béo thực vật, vitamin A,D', [], [], callback);
        },
        //sữa đặc
        function (callback) {
            taoMatHang('sp-sua-dac-ngoi-sao-phuong-nam-cam',
                'Sữa đặc Ngôi Sao Phương Nam Cam',
                1, '380g', 5000, 'Còn hàng', {
                    thanhPhan: 'Đường tinh luyện (47%), nước, dầu thực vật, whey bột, sữa bột, maltodextrin, lactoza, chất nhũ hóa, muối ăn, hương liệu tổng hợp dùng trong thực phẩm.',
                    huongDanSuDung: 'Sau khi mở lon nên đậy kín, bảo quản trong tủ lạnh và sử dụng hết trong vòng 3 ngày.',
                    baoQuan: 'Để ở nơi khô ráo thoáng mát, tránh áng nắng mặt trời.'
                },
                'Vị sữa ngọt, béo ngọt, đậm đà. Sữa bột, đường tinh luyện, bột Whey. Đóng lon nhỏ gọn, tiện dụng', [], [], callback);
        },
        function (callback) {
            taoMatHang('sp-sua-dac-ngoi-sao-phuong-nam-xanh-duong',
                'Sữa đặc Ngôi Sao Phương Nam xanh dương',
                1, '380g', 5000, 'Còn hàng', {
                    thanhPhan: 'Đường tinh luyện (47%), nước, dầu thực vật, whey bột, sữa bột, maltodextrin, lactoza, chất nhũ hóa, muối ăn, hương liệu tổng hợp dùng trong thực phẩm.',
                    huongDanSuDung: 'Sau khi mở lon nên đậy kín, bảo quản trong tủ lạnh và sử dụng hết trong vòng 3 ngày.',
                    baoQuan: 'Để ở nơi khô ráo thoáng mát, tránh áng nắng mặt trời.'
                },
                'Vị sữa ngọt, béo ngọt, đậm đà. Sữa bột, đường tinh luyện, bột Whey. Đóng lon nhỏ gọn, tiện dụng', [], [], callback);
        },
        function (callback) {
            taoMatHang('sp-sua-dac-ong-tho-do',
                'Sữa đặc Ông Thọ đỏ',
                1, '380g', 5000, 'Còn hàng', {
                    thanhPhan: 'Đường tinh luyện (47%), nước, dầu thực vật, whey bột, sữa bột, maltodextrin, lactoza, chất nhũ hóa, muối ăn, hương liệu tổng hợp dùng trong thực phẩm.',
                    huongDanSuDung: 'Sau khi mở lon nên đậy kín, bảo quản trong tủ lạnh và sử dụng hết trong vòng 3 ngày.',
                    baoQuan: 'Để ở nơi khô ráo thoáng mát, tránh áng nắng mặt trời.'
                },
                'Vị sữa ngọt, béo ngọt, đậm đà. Sữa bột, đường tinh luyện, bột Whey. Đóng lon nhỏ gọn, tiện dụng', [], [], callback);
        },
        function (callback) {
            taoMatHang('sp-sua-dac-vega-xanh',
                'Sữa đặc Vega Xanh',
                1, '380g', 5000, 'Còn hàng', {
                    thanhPhan: 'Đường tinh luyện (47%), nước, dầu thực vật, whey bột, sữa bột, maltodextrin, lactoza, chất nhũ hóa, muối ăn, hương liệu tổng hợp dùng trong thực phẩm.',
                    huongDanSuDung: 'Sau khi mở lon nên đậy kín, bảo quản trong tủ lạnh và sử dụng hết trong vòng 3 ngày.',
                    baoQuan: 'Để ở nơi khô ráo thoáng mát, tránh áng nắng mặt trời.'
                },
                'Vị sữa ngọt, béo ngọt, đậm đà. Sữa bột, đường tinh luyện, bột Whey. Đóng lon nhỏ gọn, tiện dụng', [], [], callback);
        },
        //mì
        function (callback) {
            taoMatHang('sp-mi-3-mien-bo-rau-thom',
                'Mì 3 Miền bò rau thơm',
                1, '60g', 3000, 'Còn hàng', {
                    thanhPhan: 'Mì 3 Miền Gold sợi to và dài hương vị Bò hầm rau thơm gói 60g được làm từ bột đậu xanh, bột khoai tây, bột mì, màu tự nhiên, vitamin B1, B2, B3, B6, B9... trở nên dai, mềm hấp dẫn.',
                    huongDanSuDung: 'Cho vắt mì và gói gia vị vào tô, chế khoảng 350ml nước sôi, đậy kín, chờ 3 phút trộn đều và thưởng thức.',
                    baoQuan: 'Để ở nơi khô ráo thoáng mát, tránh áng nắng mặt trời.'
                },
                'Vị bò hầm thơm ngon, hấp dẫn. Sợi mì to và dài hấp dẫn. Đóng gói nhỏ gọn, tiện dụng', [], [], callback);
        },
        function (callback) {
            taoMatHang('sp-mi-3-mien-tom-chua-cay',
                'Mì 3 Miền tôm chua cay',
                1, '60g', 3000, 'Còn hàng', {
                    thanhPhan: 'Mì 3 Miền Gold sợi to và dài hương vị Bò hầm rau thơm gói 60g được làm từ bột đậu xanh, bột khoai tây, bột mì, màu tự nhiên, vitamin B1, B2, B3, B6, B9... trở nên dai, mềm hấp dẫn.',
                    huongDanSuDung: 'Cho vắt mì và gói gia vị vào tô, chế khoảng 350ml nước sôi, đậy kín, chờ 3 phút trộn đều và thưởng thức.',
                    baoQuan: 'Để ở nơi khô ráo thoáng mát, tránh áng nắng mặt trời.'
                },
                'Vị bò hầm thơm ngon, hấp dẫn. Sợi mì to và dài hấp dẫn. Đóng gói nhỏ gọn, tiện dụng', [], [], callback);
        },
        function (callback) {
            taoMatHang('sp-mi-gau-do-bo-bit-tet',
                'Mì Gấu Đỏ bò bít tết',
                1, '60g', 3000, 'Còn hàng', {
                    thanhPhan: 'Mì 3 Miền Gold sợi to và dài hương vị Bò hầm rau thơm gói 60g được làm từ bột đậu xanh, bột khoai tây, bột mì, màu tự nhiên, vitamin B1, B2, B3, B6, B9... trở nên dai, mềm hấp dẫn.',
                    huongDanSuDung: 'Cho vắt mì và gói gia vị vào tô, chế khoảng 350ml nước sôi, đậy kín, chờ 3 phút trộn đều và thưởng thức.',
                    baoQuan: 'Để ở nơi khô ráo thoáng mát, tránh áng nắng mặt trời.'
                },
                'Vị bò hầm thơm ngon, hấp dẫn. Sợi mì to và dài hấp dẫn. Đóng gói nhỏ gọn, tiện dụng', [], [], callback);
        },
        function (callback) {
            taoMatHang('sp-mi-hao-hao-vi-tom-chua-cay',
                'Mì Hảo Hảo vị tôm chua cay',
                1, '60g', 3000, 'Còn hàng', {
                    thanhPhan: 'Mì 3 Miền Gold sợi to và dài hương vị Bò hầm rau thơm gói 60g được làm từ bột đậu xanh, bột khoai tây, bột mì, màu tự nhiên, vitamin B1, B2, B3, B6, B9... trở nên dai, mềm hấp dẫn.',
                    huongDanSuDung: 'Cho vắt mì và gói gia vị vào tô, chế khoảng 350ml nước sôi, đậy kín, chờ 3 phút trộn đều và thưởng thức.',
                    baoQuan: 'Để ở nơi khô ráo thoáng mát, tránh áng nắng mặt trời.'
                },
                'Vị bò hầm thơm ngon, hấp dẫn. Sợi mì to và dài hấp dẫn. Đóng gói nhỏ gọn, tiện dụng', [], [], callback);
        },
        //bún
        function (callback) {
            taoMatHang('sp-bun-mang-chay-an-lien-bich-chi',
                'Bún măng chay ăn liền Bích Chi',
                1, '60g', 5000, 'Còn hàng', {
                    thanhPhan: 'Sợi bún 3 được làm từ bột mì, màu tự nhiên, vitamin B1, B2, B3, B6, B9... trở nên dai, mềm hấp dẫn.',
                    huongDanSuDung: 'Cho vắt bún và gói gia vị vào tô, chế khoảng 400ml nước sôi, đậy kín, chờ 3 phút trộn đều và thưởng thức.',
                    baoQuan: 'Để ở nơi khô ráo thoáng mát, tránh áng nắng mặt trời.'
                },
                'Vị cua thơm ngon hấp dẫn. Sợi bún mềm, dai. Gói nhỏ tiện lợi', [], [], callback);
        },
        function (callback) {
            taoMatHang('sp-bun-rieu-cua-bich-chi',
                'Bún riêu cua Bích Chi',
                1, '60g', 5000, 'Còn hàng', {
                    thanhPhan: 'Sợi bún 3 được làm từ bột mì, màu tự nhiên, vitamin B1, B2, B3, B6, B9... trở nên dai, mềm hấp dẫn.',
                    huongDanSuDung: 'Cho vắt bún và gói gia vị vào tô, chế khoảng 400ml nước sôi, đậy kín, chờ 3 phút trộn đều và thưởng thức.',
                    baoQuan: 'Để ở nơi khô ráo thoáng mát, tránh áng nắng mặt trời.'
                },
                'Vị cua thơm ngon hấp dẫn. Sợi bún mềm, dai. Gói nhỏ tiện lợi', [], [], callback);
        },
        function (callback) {
            taoMatHang('sp-bun-rieu-cua-nhu-y',
                'Bún riêu cua Như Ý',
                1, '60g', 5000, 'Còn hàng', {
                    thanhPhan: 'Sợi bún 3 được làm từ bột mì, màu tự nhiên, vitamin B1, B2, B3, B6, B9... trở nên dai, mềm hấp dẫn.',
                    huongDanSuDung: 'Cho vắt bún và gói gia vị vào tô, chế khoảng 400ml nước sôi, đậy kín, chờ 3 phút trộn đều và thưởng thức.',
                    baoQuan: 'Để ở nơi khô ráo thoáng mát, tránh áng nắng mặt trời.'
                },
                'Vị cua thơm ngon hấp dẫn. Sợi bún mềm, dai. Gói nhỏ tiện lợi', [], [], callback);
        },
        function (callback) {
            taoMatHang('sp-bun-tom-thai-bich-chi',
                'Bún tôm thái Bích Chi',
                1, '60g', 5000, 'Còn hàng', {
                    thanhPhan: 'Sợi bún 3 được làm từ bột mì, màu tự nhiên, vitamin B1, B2, B3, B6, B9... trở nên dai, mềm hấp dẫn.',
                    huongDanSuDung: 'Cho vắt bún và gói gia vị vào tô, chế khoảng 400ml nước sôi, đậy kín, chờ 3 phút trộn đều và thưởng thức.',
                    baoQuan: 'Để ở nơi khô ráo thoáng mát, tránh áng nắng mặt trời.'
                },
                'Vị cua thơm ngon hấp dẫn. Sợi bún mềm, dai. Gói nhỏ tiện lợi', [], [], callback);
        },
        //bột ngọt
        function (callback) {
            taoMatHang('sp-bot-ngot-ajinomoto',
                'Bột ngọt Ajinomoto',
                1, '100g', 15000, 'Còn hàng', {
                    thanhPhan: 'Bột ngọt Miwon 100g được sản xuất bằng phương pháp lên men tự nhiên từ nguyên liệu tự nhiên được chọn lọc kỹ lưỡng là mật mía đường và tinh bột khoai mì.',
                    huongDanSuDung: 'Dùng làm gia vị chế biến các món ăn.',
                    baoQuan: 'Để ở nơi khô ráo thoáng mát, tránh áng nắng mặt trời.'
                },
                'Làm từ mật mía đường nguyên chất. Chứa axit glutamic giúp tăng hương vị. Sản xuất tại Việt Nam', [], [], callback);
        },
        function (callback) {
            taoMatHang('sp-bot-ngot-a-one',
                'Bột ngọt A One',
                1, '100g', 15000, 'Còn hàng', {
                    thanhPhan: 'Bột ngọt Miwon 100g được sản xuất bằng phương pháp lên men tự nhiên từ nguyên liệu tự nhiên được chọn lọc kỹ lưỡng là mật mía đường và tinh bột khoai mì.',
                    huongDanSuDung: 'Dùng làm gia vị chế biến các món ăn.',
                    baoQuan: 'Để ở nơi khô ráo thoáng mát, tránh áng nắng mặt trời.'
                },
                'Làm từ mật mía đường nguyên chất. Chứa axit glutamic giúp tăng hương vị. Sản xuất tại Việt Nam', [], [], callback);
        },
        function (callback) {
            taoMatHang('sp-bot-ngot-miwon',
                'Bột ngọt Miwon',
                1, '100g', 15000, 'Còn hàng', {
                    thanhPhan: 'Bột ngọt Miwon 100g được sản xuất bằng phương pháp lên men tự nhiên từ nguyên liệu tự nhiên được chọn lọc kỹ lưỡng là mật mía đường và tinh bột khoai mì.',
                    huongDanSuDung: 'Dùng làm gia vị chế biến các món ăn.',
                    baoQuan: 'Để ở nơi khô ráo thoáng mát, tránh áng nắng mặt trời.'
                },
                'Làm từ mật mía đường nguyên chất. Chứa axit glutamic giúp tăng hương vị. Sản xuất tại Việt Nam', [], [], callback);
        },
        function (callback) {
            taoMatHang('sp-bot-ngot-vedan',
                'Bột ngọt Vedan',
                1, '100g', 15000, 'Còn hàng', {
                    thanhPhan: 'Bột ngọt Miwon 100g được sản xuất bằng phương pháp lên men tự nhiên từ nguyên liệu tự nhiên được chọn lọc kỹ lưỡng là mật mía đường và tinh bột khoai mì.',
                    huongDanSuDung: 'Dùng làm gia vị chế biến các món ăn.',
                    baoQuan: 'Để ở nơi khô ráo thoáng mát, tránh áng nắng mặt trời.'
                },
                'Làm từ mật mía đường nguyên chất. Chứa axit glutamic giúp tăng hương vị. Sản xuất tại Việt Nam', [], [], callback);
        },
        //hạt nêm
        function (callback) {
            taoMatHang('sp-hat-nem-aji-ngon-heo-goi',
                'Hạt nêm Aji Ngon Heo',
                1, '60g', 5000, 'Còn hàng', {
                    thanhPhan: 'Thành phần từ muối, chất điều vị (Mononatri Glutamat 621, Dinatri Inosinat 631, Dinatri Guanylat 627), đường, tinh bột, chiết xuất cô đặc (từ thịt, xương ống và xương sườn) 3.3%, Canxi Carbonat',
                    huongDanSuDung: '1 muỗng cho 500 thịt/cá.',
                    baoQuan: 'Để ở nơi khô ráo thoáng mát, tránh áng nắng mặt trời.'
                },
                'Chiết xuất từ xương và thịt. Dạng gói dễ sử dụng, bảo quản. Bảo đảm an toàn vệ sinh thực phẩm.', [], [], callback);
        },
        function (callback) {
            taoMatHang('sp-hat-nem-aji-ngon-nam-huong-hat-sen',
                'Hạt nêm Aji Ngon nấm hương hạt sen',
                1, '60g', 5000, 'Còn hàng', {
                    thanhPhan: 'Thành phần từ muối, chất điều vị (Mononatri Glutamat 621, Dinatri Inosinat 631, Dinatri Guanylat 627), đường, tinh bột, chiết xuất cô đặc (từ thịt, xương ống và xương sườn) 3.3%, Canxi Carbonat',
                    huongDanSuDung: '1 muỗng cho 500 thịt/cá.',
                    baoQuan: 'Để ở nơi khô ráo thoáng mát, tránh áng nắng mặt trời.'
                },
                'Chiết xuất từ xương và thịt. Dạng gói dễ sử dụng, bảo quản. Bảo đảm an toàn vệ sinh thực phẩm.', [], [], callback);
        },
        function (callback) {
            taoMatHang('sp-hat-nem-knorr-3-nam-ngon',
                'Hạt nêm Knorr 3 Nấm',
                1, '60g', 5000, 'Còn hàng', {
                    thanhPhan: 'Thành phần từ muối, chất điều vị (Mononatri Glutamat 621, Dinatri Inosinat 631, Dinatri Guanylat 627), đường, tinh bột, chiết xuất cô đặc (từ thịt, xương ống và xương sườn) 3.3%, Canxi Carbonat',
                    huongDanSuDung: '1 muỗng cho 500 thịt/cá.',
                    baoQuan: 'Để ở nơi khô ráo thoáng mát, tránh áng nắng mặt trời.'
                },
                'Chiết xuất từ xương và thịt. Dạng gói dễ sử dụng, bảo quản. Bảo đảm an toàn vệ sinh thực phẩm.', [], [], callback);
        }
    ], cb)
}

//tạo dữ liệu danh mục
function taoDuLieuMau2(cb) {
    async.series([
        function (callback) {
            //tạo danh mục cấp 2 Nước tinh khiết 
            let thuongHieu1 = {
                maSo: 'th-lavie',
                ten: 'Lavie',
                idMatHang: [dsMatHang[1]._id]
            }
            let thuongHieu2 = {
                maSo: 'th-vinh-hao',
                ten: 'Vĩnh Hảo',
                idMatHang: [dsMatHang[2]._id]
            }
            let thuongHieu3 = {
                maSo: 'th-danasi',
                ten: 'Danasi',
                idMatHang: [dsMatHang[3]._id]
            }
            let thuongHieu4 = {
                maSo: 'th-aquafina',
                ten: 'Aquafina',
                idMatHang: [dsMatHang[0]._id]
            }
            let danhMucCap2NuocTinhKhiet = {
                maSo: 'dm2-nuoc-tinh-khiet',
                ten: 'Nước tinh khiết',
                thuongHieu: [thuongHieu1, thuongHieu2, thuongHieu3, thuongHieu4]
            }
            //thêm danh mục cấp 2 bia
            let thuongHieu5 = {
                maSo: 'th-su-tu-trang',
                ten: 'Sư tử trắng',
                idMatHang: [dsMatHang[7]._id]
            }
            let thuongHieu6 = {
                maSo: 'th-sai-gon',
                ten: 'Sài Gòn',
                idMatHang: [dsMatHang[4]._id, dsMatHang[5]._id, dsMatHang[6]._id]
            }
            let danhMucCap2Bia = {
                maSo: 'dm2-bia',
                ten: 'Bia',
                thuongHieu: [thuongHieu5, thuongHieu6]
            }
            //thêm danh mục cấp 2 vào danh mục cấp 1 tương ứng
            taoDanhMucCap1('dm1-do-uong', "Đồ uống", [danhMucCap2NuocTinhKhiet, danhMucCap2Bia], callback)
        },
        function (callback) {
            //thêm danh mục cấp 2 sữa tươi
            let thuongHieu1 = {
                maSo: 'th-dutch-lady',
                ten: 'Dutch Lady',
                idMatHang: [dsMatHang[8]._id]
            }
            let thuongHieu2 = {
                maSo: 'th-vinamilk',
                ten: 'Vinamilk',
                idMatHang: [dsMatHang[10]._id, dsMatHang[11]._id]
            }
            let thuongHieu3 = {
                maSo: 'th-nutifood',
                ten: 'Nutifood',
                idMatHang: [dsMatHang[9]._id]
            }
            let danhMucCap2SuaTuoi = {
                maSo: 'dm2-sua-tuoi',
                ten: 'Sữa tươi',
                thuongHieu: [thuongHieu1, thuongHieu2, thuongHieu3]
            }
            //thêm danh mục cấp 2 sữa đặc
            let thuongHieu4 = {
                maSo: 'th-ong-tho',
                ten: 'Ông Thọ',
                idMatHang: [dsMatHang[14]._id]
            }
            let thuongHieu5 = {
                maSo: 'th-ngoi-sao-phuong-nam',
                ten: 'Ngôi sao Phương Nam',
                idMatHang: [dsMatHang[12]._id, dsMatHang[13]._id]
            }
            let thuongHieu6 = {
                maSo: 'th-vega',
                ten: 'Vega',
                idMatHang: [dsMatHang[15]._id]
            }
            let danhMucCap2SuaDac = {
                maSo: 'dm2-sua-dac',
                ten: 'Sữa đặc',
                thuongHieu: [thuongHieu4, thuongHieu5, thuongHieu6]
            }
            //thêm danh mục cấp 2 vào danh mục cấp 1 tương ứng
            taoDanhMucCap1('dm1-sua', "Sữa", [danhMucCap2SuaTuoi, danhMucCap2SuaDac], callback)
        },
        function (callback) {
            //thêm danh mục cấp 2 mì
            let thuongHieu1 = {
                maSo: 'th-3-mien',
                ten: '3 Miền',
                idMatHang: [, dsMatHang[16]._id, dsMatHang[17]._id]
            }
            let thuongHieu2 = {
                maSo: 'th-gau-do',
                ten: 'Gấu Đỏ',
                idMatHang: [dsMatHang[18]._id]
            }
            let thuongHieu3 = {
                maSo: 'th-hao-hao',
                ten: 'Hảo Hảo',
                idMatHang: [dsMatHang[19]._id]
            }
            let danhMucCap2Mi = {
                maSo: 'dm2-mi',
                ten: 'Mì',
                thuongHieu: [thuongHieu1, thuongHieu2, thuongHieu3]
            }
            //thêm danh mục cấp 2 bún
            let thuongHieu4 = {
                maSo: 'th-bich-chi',
                ten: 'Bích Chi',
                idMatHang: [dsMatHang[20]._id, dsMatHang[21]._id, dsMatHang[23]._id]
            }
            let thuongHieu5 = {
                maSo: 'th-nhu-y',
                ten: 'Như Ý',
                idMatHang: [dsMatHang[22]._id]
            }
            let danhMucCap2Bun = {
                maSo: 'dm2-bun',
                ten: 'Bún',
                thuongHieu: [thuongHieu4, thuongHieu5]
            }
            //thêm danh mục cấp 2 vào danh mục cấp 1 tương ứng
            taoDanhMucCap1('dm1-thuc-pham-an-lien', "Thực phẩm ăn liền", [danhMucCap2Bun, danhMucCap2Mi], callback)
        },
        function (callback) {
            //thêm danh mục cấp 2 bột ngọt
            let thuongHieu1 = {
                maSo: 'th-ajinomoto',
                ten: 'Ajinomoto',
                idMatHang: [dsMatHang[24]._id]
            }
            let thuongHieu2 = {
                maSo: 'th-a-one',
                ten: 'A One',
                idMatHang: [dsMatHang[25]._id]
            }
            let thuongHieu3 = {
                maSo: 'th-miwon',
                ten: 'Miwon',
                idMatHang: [dsMatHang[26]._id]
            }
            let thuongHieu4 = {
                maSo: 'th-vedan',
                ten: 'Vedan',
                idMatHang: [dsMatHang[27]._id]
            }
            let danhMucCap2BotNgot = {
                maSo: 'dm2-bot-ngot',
                ten: 'Bột ngọt',
                thuongHieu: [thuongHieu1, thuongHieu2, thuongHieu3, thuongHieu4]
            }
            //thêm danh mục cấp 2 hạt nêm
            let thuongHieu5 = {
                maSo: 'th-aji-ngon',
                ten: 'Aji Ngon',
                idMatHang: [dsMatHang[28]._id, dsMatHang[29]._id]
            }
            let thuongHieu6 = {
                maSo: 'th-knorr',
                ten: 'Knorr',
                idMatHang: [dsMatHang[30]._id]
            }
            let danhMucCap2HatNem = {
                maSo: 'dm2-hat-nem',
                ten: 'Hạt nêm',
                thuongHieu: [thuongHieu5, thuongHieu6]
            }
            //thêm danh mục cấp 2 vào danh mục cấp 1 tương ứng
            taoDanhMucCap1('dm1-gia-vi-nau-an', "Gia vị nấu ăn", [danhMucCap2HatNem, danhMucCap2BotNgot], callback)
        },
        function (callback) {
            taoDonHang(dsNguoiDung[0]._id, '2018/04/30', 'DH KHTN', [{
                idMatHang: dsMatHang[0],
                soLuong: 1,
                gia: 45000,
                thanhTien: 45000
            }], 45000, 'Chưa xử lí', '', '', callback);
        }
    ], cb)
}

//hàm gọi tuần tự các hàm (2)
async.series([taoDuLieuMau1, taoDuLieuMau2],
    // Callback của async.series, chạy sau cùng khi các hàm trên chạy xong
    (err, results) => {
        if (err) {
            console.log('FINAL ERR: ' + err);
        } else {
            console.log('Tao du lieu mau thanh cong');
        }
        // Xong hết tất cả thì đóng connection lại
        mongoose.connection.close();
    })

// async.series([
//         createGenreAuthors,
//         createBooks,
//         createBookInstances
//     ],
//     // Callback của async.series, chạy sau cùng khi các hàm trên chạy xong
//     function (err, results) {
//         if (err) {
//             console.log('FINAL ERR: ' + err);
//         } else {
//             console.log('BOOKInstances: ' + bookinstances);

//         }
//         // Xong hết tất cả thì đóng connection lại
//         mongoose.connection.close();
//     });