var createError = require('http-errors');
var express = require('express');
var path = require('path');
var cookieParser = require('cookie-parser');
var logger = require('morgan');
var config = require('./config.js');
var session = require('express-session');
var passport = require('passport');
var flash = require('connect-flash');
var hbs = require("hbs")
hbs.registerHelper("equal", require("handlebars-helper-equal"))
const DBDanhMucCap1 = require('./models/DanhMucCap1')
const NguoiDungChuaDangNhap = require('./models/NguoiDungChuaDangNhap');

var indexRouter = require('./routes/index');
var adminRouter = require('./routes/admin');
const userRouter = require('./routes/user')

//Import the mongoose module
var mongoose = require('mongoose');
//Set up default mongoose connection
var mongoDB = config.mongodb;
mongoose.connect(mongoDB);
// Get Mongoose to use the global promise library
mongoose.Promise = global.Promise;
//Get the default connection
var db = mongoose.connection;

//Bind connection to error event (to get notification of connection errors)
db.on('error', console.error.bind(console, 'MongoDB connection error:'));

var app = express();

// view engine setup
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'hbs');

app.use(logger('dev'));
app.use(express.json());
app.use(express.urlencoded({ extended: false }));
app.use(cookieParser());
app.use(express.static(path.join(__dirname, 'public')));
// Connect Flash



app.use(session({
  secret: '765364A34D9AA1CD85B96FE3777B4',
  saveUninitialized: true,
  resave: true
}))

app.use(passport.initialize());
app.use(passport.session());

app.use((req, res, next) => {
  if (req.user || req.cookies.userid == "") { //nếu đã đăng nhập thì xóa id của người dùng chưa đăng nhập đi
    res.clearCookie('userid');
    return next();
  }
  if (req.cookies.userid) {
    NguoiDungChuaDangNhap.findById(req.cookies.userid, (err, result) => {
      if (err) console.log(err);
      else req.userChuaDangNhap = result;
      return next();
    })
  } else {
    let userChuaDangNhap = new NguoiDungChuaDangNhap({
      gioHang: []
    });
    userChuaDangNhap.save((err, result) => {
      if (err) console.log(err);
      else {
        //tạm thòi set cookie cho đến khi browser đóng
        res.cookie('userid', result._id, {
          expires: 0
        });
        req.userChuaDangNhap = userChuaDangNhap;
      }
      next();
    })
  }
})

app.use(flash());

app.use('/', userRouter);

app.use('/admin', adminRouter);
app.use('/', indexRouter);

// catch 404 and forward to error handler
app.use(function(req, res, next) {
  next(createError(404));
});

// error handler
app.use(function(err, req, res, next) {
  // set locals, only providing error in development
  res.locals.message = err.message;
  res.locals.error = req.app.get('env') === 'development' ? err : {};
  // render the error page
  DBDanhMucCap1.find((err1, data) => {
    res.status(err.status || 500);
    res.render('error',{dsDanhMucCap1: data, user: req.user});
  })
});

module.exports = app;
