const themNguoiDung = form => {
  let error = form.getElementsByTagName("label")[4];
  error.innerHTML = `<i class="fas fa-spinner fa-spin"></i>`;
  if (form.hoVaTen.value == "") {
    error.innerText = "Không được để trống họ và tên";
    return false;
  }
  if (form.email.value == "") {
    error.innerText = "Không được để trống email";
    return false;
  }
  if (form.matKhau.value == "") {
    error.innerText = "Không được để trống mật khẩu";
    return false;
  }
  if (form.nhaplaimatkhau.value == "") {
    error.innerText = "Không được để trống nhập lại mật khẩu";
    return false;
  }
  if (form.matKhau.value != form.nhaplaimatkhau.value) {
    error.innerText = "Mật khẩu và mật khẩu nhập lại không khớp";
    return false;
  }
  $.post(
    "/admin/nguoi-dung/them-nguoi-dung",
    {
      hoVaTen: form.hoVaTen.value,
      email: form.email.value,
      matKhau: form.matKhau.value
    },
    data => {
      data = JSON.parse(data);
      if (data.status == true) {
        location.reload();
      } else {
        error.innerText = data.message;
      }
    }
  );
  return false;
};

const capNhatNguoiDung = form => {
  let error = form.getElementsByTagName("label")[2];
  error.innerHTML = `<i class="fas fa-spinner fa-spin"></i>`;
  if (form.hoVaTen.value == "") {
    error.innerText = "Không được để trống họ và tên";
    return false;
  }
  if (form.email.value == "") {
    error.innerText = "Không được để trống email";
    return false;
  }
  $.post(
    "/admin/nguoi-dung/cap-nhat-nguoi-dung",
    {
      hoVaTen: form.hoVaTen.value,
      email: form.email.value
    },
    data => {
      data = JSON.parse(data);
      if (data.status == true) {
        location.reload();
      } else {
        error.innerText = data.message;
      }
    }
  );
  return false;
};

const resetPassWord = (email, idSpan) => {
  if (email == undefined || email == "") {
    return false;
  }
  let error = document.getElementById(idSpan);
  error.innerHTML = `<i class="fas fa-spinner fa-spin"></i>`;
  $.post(
    "/admin/nguoi-dung/reset-password-nguoi-dung",
    {
      email: email
    },
    data => {
      data = JSON.parse(data);
      error.innerText = data.message;
    }
  );
};

const resetPassWordAdmin = (email, idSpan) => {
  if (email == undefined || email == "") {
    return false;
  }
  let error = document.getElementById(idSpan);
  error.innerHTML = `<i class="fas fa-spinner fa-spin"></i>`;
  $.post(
    "/admin/admin/reset-password-admin",
    {
      email: email
    },
    data => {
      data = JSON.parse(data);
      if (data.status == true) {
        error.innerText = "đã đổi thành 123456";
      } else {
        error.innerText = data.message;
      }
    }
  );
};

const deleteNguoiDung = (email, idError) => {
  if (email == undefined || email == "") {
    return false;
  }
  let error = document.getElementById(idError);
  error.innerHTML = `<i class="fas fa-spinner fa-spin "></i>`;
  $.post(
    "/admin/nguoi-dung/xoa-nguoi-dung",
    {
      email: email
    },
    data => {
      data = JSON.parse(data);
      if (data.status == true) {
        location.reload();
      } else {
        error.innerText = data.message;
      }
    }
  );
};


const themAdmin = form => {
  let error = form.getElementsByTagName("label")[5];
  error.innerHTML = `<i class="fas fa-spinner fa-spin"></i>`;
  if (form.hoVaTen.value == "") {
    error.innerText = "Không được để trống họ và tên";
    return false;
  }
  if (form.email.value == "") {
    error.innerText = "Không được để trống email";
    return false;
  }
  if (form.matKhau.value == "") {
    error.innerText = "Không được để trống mật khẩu";
    return false;
  }
  if (form.nhaplaimatkhau.value == "") {
    error.innerText = "Không được để trống nhập lại mật khẩu";
    return false;
  }
  if (form.matKhau.value != form.nhaplaimatkhau.value) {
    error.innerText = "Mật khẩu và mật khẩu nhập lại không khớp";
    return false;
  }
  $.post(
    "/admin/admin/them-admin",
    {
      hoVaTen: form.hoVaTen.value,
      email: form.email.value,
      matKhau: form.matKhau.value,
      super: form.super.value
    },
    data => {
      data = JSON.parse(data);
      if (data.status == true) {
        location.reload();
      } else {
        error.innerText = data.message;
      }
    }
  );
  return false;
};

const deleteAdmin = (email, idError) => {
  if (email == undefined || email == "") {
    return false;
  }
  let error = document.getElementById(idError);
  error.innerHTML = `<i class="fas fa-spinner fa-spin "></i>`;
  $.post(
    "/admin/admin/xoa-admin",
    {
      email: email
    },
    data => {
      data = JSON.parse(data);
      if (data.status == true) {
        location.reload();
      } else {
        error.innerText = data.message;
      }
    }
  );
};

const capNhatAdmin = form => {
  let error = form.getElementsByTagName("label")[3];
  error.innerHTML = `<i class="fas fa-spinner fa-spin"></i>`;
  if (form.hoVaTen.value == "") {
    error.innerText = "Không được để trống họ và tên";
    return false;
  }
  if (form.email.value == "") {
    error.innerText = "Không được để trống email";
    return false;
  }
  if (form.super.value == "") {
    error.innerText = "Không được để trống super";
    return false;
  }
  $.post(
    "/admin/admin/cap-nhat-admin",
    {
      hoVaTen: form.hoVaTen.value,
      email: form.email.value,
      super: form.super.value
    },
    data => {
      data = JSON.parse(data);
      if (data.status == true) {
        location.reload();
      } else {
        error.innerText = data.message;
      }
    }
  );
  return false;
};