const account = require('../config').mailler;

var nodemailer = require('nodemailer');
var hbsMailler = require('nodemailer-express-handlebars');

var transporter = nodemailer.createTransport({
    service: 'gmail',
    auth: {
        user: account.user,
        pass: account.pass
    }
});

transporter.use('compile', hbsMailler({
    viewPath: 'views/mailler',
    extName: '.hbs'
}));

module.exports = {
    //template, context, desMail, subject
    sendMail: (template, context, desMail, subject) => {
        const mailOptions = {
            from: 'Bách hóa ba con thỏ', // sender address
            to: desMail, // list of receivers
            subject: subject, // Subject line
            // html: '<p>Your html here</p>' // plain text body
            template: template,
            context: context
        };
        
        transporter.sendMail(mailOptions, function (err, info) {
            if(err)
              console.log(err)
            else
              console.log(info);
        });
    }
}