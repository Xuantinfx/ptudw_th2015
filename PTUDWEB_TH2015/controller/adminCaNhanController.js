const QuanTriVien = require('../models/QuanTriVien')
const bcrypt = require('bcryptjs')

module.exports.getCaNhan = (req, res, next) => {
    res.render("admin/canhan", {layout: "admin/layout",user: req.user, caNhan: true});
}

module.exports.postDoiTen = (req, res, next) => {
    let hoVaTen = req.body.hoVaTen;
    if (hoVaTen == undefined || hoVaTen == '') {
        res.end(JSON.stringify({
            status: "error",
            message: "Họ và tên không được rỗng"
        }))
    }

    QuanTriVien.update({
        email: req.user.email
    }, {
        hoVaTen: hoVaTen
    }, (err, raw) => {
        if (err) {
            res.end(JSON.stringify({
                status: "error",
                message: "Lỗi đọc DB"
            }))
        }

        res.end(JSON.stringify({
            status: "success",
            message: "Họ và tên đã được đổi thành " + hoVaTen
        }))

    })
}

module.exports.postDoiMatKhau = (req, res, next) => {
    let matKhauCu = req.body.matKhauCu;
    let matKhauMoi = req.body.matKhauMoi;
    let reMatKhauMoi = req.body.reMatKhauMoi;

    if (matKhauCu == undefined || matKhauCu == '') {
        res.end(JSON.stringify({
            status: "error",
            message: "Mật khẩu cũ không được rỗng"
        }))
    }
    if (matKhauMoi == undefined || matKhauMoi == '') {
        res.end(JSON.stringify({
            status: "error",
            message: "Mật khẩu mới không được rỗng"
        }))
    }
    if (reMatKhauMoi == undefined || reMatKhauMoi == '') {
        res.end(JSON.stringify({
            status: "error",
            message: "Nhập lại mật khẩu mới không được rỗng"
        }))
    }

    bcrypt.compare(matKhauCu, req.user.matKhau, function (err, isMatch) {
        if (err) return res.end(JSON.stringify({
            status: "error",
            message: "Lỗi so sánh mật khẩu"
        }))
        if (isMatch) {
            if(matKhauMoi != reMatKhauMoi) {
                return res.end(JSON.stringify({
                    status: "error",
                    message: "Mật khẩu mới và nhập lại mật khẩu mới không khớp"
                }))
            }
            bcrypt.genSalt(10, function (err, salt) {
                bcrypt.hash(matKhauMoi, salt, function (err, hash) {
                    matKhauMoi = hash;
                    QuanTriVien.update({email: req.user.email}, {matKhau: matKhauMoi}, (err, raw) => {
                        if(err) {
                            return res.end(JSON.stringify({
                                status: "error",
                                message: "Lỗi cập nhật mật khẩu"
                            }))
                        }
                        return res.end(JSON.stringify({
                            status: "success",
                            message: "Mật khẩu đã được thay đổi"
                        }))
                    })
                });
            });
        } else  return res.end(JSON.stringify({
            status: "error",
            message: "Sai mật khẩu cũ"
        }))
    });
}