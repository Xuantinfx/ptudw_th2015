const MatHang = require('../models/MatHang')
const DonHang = require('../models/DonHang')

const top10SanPhamBanChayNhat = () => {
    return new Promise((resolve, reject) => {
        DonHang.find({}).populate("danhSachMua.idMatHang").exec((err, result) => {
            if(err) {
                resolve([])
                return;
            }
            let dsMatHangDaDuocMua = [];
            for(let i = 0; i < result.length; i++) {
                for(let j = 0; j < result[i].danhSachMua.length; j++) {
                    let temp = result[i].danhSachMua[j];
                    dsMatHangDaDuocMua.push({...temp.idMatHang.toObject(), soLuong: temp.soLuong});
                }
            }

            let length = dsMatHangDaDuocMua.length;
            for(let i = 0; i < length - 1; i++) {
                for(let j = i + 1; j < length; j++) {
                    if(dsMatHangDaDuocMua[i]._id.toString() == dsMatHangDaDuocMua[j]._id.toString()) {
                        dsMatHangDaDuocMua[i].soLuong += dsMatHangDaDuocMua[j].soLuong;
                        dsMatHangDaDuocMua.splice(j,1);
                        length--;
                        j--;
                    }
                }
            }
            dsMatHangDaDuocMua.sort((a,b) => {
                return parseInt(b.soLuong) > parseInt(a.soLuong);
            })
            
            resolve(dsMatHangDaDuocMua.slice(0,10));
        })
    })
}

module.exports.getThongKe = (req, res, next) => {
    top10SanPhamBanChayNhat()
    .then(result => {
        console.log(result.length)
        res.render("admin/thongke", {layout: 'admin/layout', user: req.user, thongKe: true, ds10SpBanChay: result});
    })
}