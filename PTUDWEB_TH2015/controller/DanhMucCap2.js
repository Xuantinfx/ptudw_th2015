const mongoose = require("mongoose");
const DBDanhMucCap1 = require('../models/DanhMucCap1.js')
const DBMatHang = require('../models/MatHang')
const ObjectId = mongoose.Types.ObjectId;

module.exports.GetDanhMucCap2 = (req, res, next) => {
  DBDanhMucCap1.find((err, data) => {
    if (err) {
      console.log(err + "");
      return next();
    }

    // //lay cac danh muc cap 1
    // let dsDanhMucCap1 = data.map(item => {
    //   return {
    //     maSo: item.maSo,
    //     ten: item.ten
    //   }
    // })

    let dsDanhMucCap1 = data;

    //lay danh muc cap 1
    let danhMucCap1 = undefined;
    for (let i = 0; i < data.length; i++) {
      if (data[i].maSo === req.params.dm1) {
        danhMucCap1 = data[i];
        break;
      }
    }

    if (danhMucCap1 === undefined) {
      console.log('loi danh muc cap 1')
      return next();
    }

    if (danhMucCap1.danhMucCap2.length == 0) {
      return next();
    }
    //lay danh muc cap 2
    let danhMucCap2 = undefined;
    for (let i = 0; i < danhMucCap1.danhMucCap2.length; i++) {
      if (danhMucCap1.danhMucCap2[i].maSo === req.params.dm2) {
        danhMucCap2 = danhMucCap1.danhMucCap2[i];
      }
    }
    if (danhMucCap2 === undefined) {
      return next();
    }
    //lay danh sach thuong hieu
    let dsThuongHieu = danhMucCap2.thuongHieu;

    //danh sach san pham
    let dsIDSanPham = [];
    for (let i = 0; i < dsThuongHieu.length; i++) {
      for (let j = 0; j < dsThuongHieu[i].idMatHang.length; j++) {
        dsIDSanPham.push(dsThuongHieu[i].idMatHang[j])
      }
    }
    DBMatHang.find({
      "_id": {
        "$in": dsIDSanPham
      }
    }).exec((err, dsSP) => {
      if (err) {
        console.log(err + '');
        return next()
      }
      //render
      res.render('danhmuccap2', {
        dsDanhMucCap1: req.dsDanhMucCap1,
        danhMucCap2,
        dsThuongHieu,
        dsSP,
        user: req.user,
        tongSoLuongMatHangTrongGio: req.tongSoLuongMatHangTrongGio
      });
    })
  });
};

//dùng kèm theo để hỗ trợ render ra danh mục trên navbar
module.exports.themDanhMucVaoNavBar = function (req, res, next) {
  let gioHang = [];
  if (req.user)
    gioHang = req.user.gioHang;
  else gioHang = req.userChuaDangNhap.gioHang;
  let tongSoLuongMatHangTrongGio = 0;
  for (let i = 0; i < gioHang.length; i++) {
    tongSoLuongMatHangTrongGio += gioHang[i].soLuong;
  }
  if (tongSoLuongMatHangTrongGio != 0) req.tongSoLuongMatHangTrongGio = tongSoLuongMatHangTrongGio;
  DBDanhMucCap1.find((err, data) => {
    if (err) {
      console.log(err);
    } else {
      req.dsDanhMucCap1 = data;
    }
    next();
  })
}