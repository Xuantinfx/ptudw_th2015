var DanhMucCap1 = require('../models/DanhMucCap1.js');
var DonHang = require('../models/DonHang');
var MatHang = require('../models/MatHang');
var NguoiDung = require('../models/NguoiDung');
var NguoiDungChuaDangNhap = require('../models/NguoiDungChuaDangNhap');
var QuanTriVien = require('../models/QuanTriVien');

var async = require('async');

exports.getChiTietMatHang = function (req, res, next) {
    let query = MatHang.findOne({
        'maSo': req.params.id
    });
    query.exec((err, result) => {
        if (result != null) {
            res.render('chitiet', {
                dsDanhMucCap1: req.dsDanhMucCap1,
                matHang: result,
                user: req.user,
                tongSoLuongMatHangTrongGio: req.tongSoLuongMatHangTrongGio
            })
        }
    });
    // async.parallel([
    //     function (callback) {
    //         //Lấy các thông tin tương ứng
    //         let query = DanhMucCap1.find({});
    //         query.exec(callback);
    //     },
    //     function (callback) {
    //         let query = MatHang.findOne({
    //             'maSo': req.params.id
    //         });
    //         query.exec(callback);
    //     }
    // ], function (err, data) {
    //     console.log(data[1])
    //     if (data[1].length == 0) {
    //         return next();
    //     }
    //     if (data != null) {
    //         // console.log(data[1][0])
    //         res.render('chitiet', {
    //             dsDanhMucCap1: data[0],
    //             matHang: data[1][0],
    //             user: req.user
    //         })
    //     }
    // })

}