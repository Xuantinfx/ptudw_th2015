const DonHang = require('../models/DonHang')

const trangThaiDonHang = {
    CHUA_XU_LY: "Chưa xử lí",
    DANG_GIAO: "Đang giao",
    DA_XU_LI: "Đã xử lí",
    HUY_DON: "Hủy đơn"
}

const mailler = require('./mailler')

module.exports.getDonHang = (req, res, next) => {
    const cb = (err, results) => {
        if (err) {
            console.log(err);
            res.redirect("/dashboar");
        } else {
            let dsDonHangChoXuLi = [],
                dsDonHangDangGiao = [],
                dsDonHangDaXuLi = [];

            for (let i = 0; i < results.length; i++) {
                switch (results[i].trangThai) {
                    case trangThaiDonHang.CHUA_XU_LY:
                        dsDonHangChoXuLi.push(results[i])
                        break;
                    case trangThaiDonHang.DANG_GIAO:
                        dsDonHangDangGiao.push(results[i])
                        break;
                    case trangThaiDonHang.DA_XU_LI:
                        dsDonHangDaXuLi.push(results[i])
                        break;
                    case trangThaiDonHang.HUY_DON:
                        dsDonHangDaXuLi.push(results[i])
                        break;
                }
            }
            //res.end(JSON.stringify({dsDonHangChoXuLi, dsDonHangDangGiao, dsDonHangDaXuLi}))
            res.render("admin/donhang", {
                layout: "admin/layout",
                donHang: true,
                dsDonHangChoXuLi, dsDonHangDangGiao, dsDonHangDaXuLi,
                user: req.user
            })
        }
    }

    DonHang.find({}).populate("nguoiMua").populate("danhSachMua.idMatHang").exec(cb)
}

module.exports.getChapNhanDonHang = (req, res, next) => {
    let id = req.query.id;
    if(!id) {
        res.redirect('/admin/don-hang')
        return;
    }
    DonHang.findByIdAndUpdate(id, {trangThai: trangThaiDonHang.DANG_GIAO}, {upsert: false})
    .populate("nguoiMua")
    .populate("danhSachMua.idMatHang")
    .exec((err, result) => {
        if(!err) {
            let emailNguoiMua = result.nguoiMua.email;
            let ten = result.nguoiMua.hoVaTen,
            dsHang = result.danhSachMua;
            mailler.sendMail('chapnhandonhang', {ten, dsHang}, emailNguoiMua, "Đơn hàng đã được chấp nhận")
        }
        res.redirect('/admin/don-hang')
    } )
}

module.exports.getGiaoHangThanhCong = (req, res, next) => {
    let id = req.query.id;
    if(!id) {
        res.redirect('/admin/don-hang')
        return;
    }
    DonHang.findByIdAndUpdate(id, {trangThai: trangThaiDonHang.DA_XU_LI}, {upsert: false})
    .populate("nguoiMua")
    .populate("danhSachMua.idMatHang")
    .exec((err, result) => {
        if(!err) {
            let emailNguoiMua = result.nguoiMua.email;
            let ten = result.nguoiMua.hoVaTen,
            dsHang = result.danhSachMua;
            mailler.sendMail('thanhcongdonhang', {ten, dsHang}, emailNguoiMua, "Đơn hàng đã được giao")
        }
        res.redirect('/admin/don-hang')
    } )
}

module.exports.getTuChoiDonHang = (req, res, next) => {
    let id = req.query.id;
    if(!id) {
        res.redirect('/admin/don-hang')
        return;
    }
    DonHang.findByIdAndUpdate(id, {trangThai: trangThaiDonHang.HUY_DON}, {upsert: false})
    .populate("nguoiMua")
    .populate("danhSachMua.idMatHang")
    .exec((err, result) => {
        if(!err) {
            let emailNguoiMua = result.nguoiMua.email;
            let ten = result.nguoiMua.hoVaTen,
            dsHang = result.danhSachMua;
            mailler.sendMail('tuchoidonhang', {ten, dsHang}, emailNguoiMua, "Đơn hàng đã bị từ chối")
        }
        res.redirect('/admin/don-hang')
    } )
}

module.exports.getHuyGiaoHang = (req, res, next) => {
    let id = req.query.id;
    if(!id) {
        res.redirect('/admin/don-hang')
        return;
    }
    DonHang.findByIdAndUpdate(id, {trangThai: trangThaiDonHang.HUY_DON}, {upsert: false})
    .populate("nguoiMua")
    .populate("danhSachMua.idMatHang")
    .exec((err, result) => {
        if(!err) {
            let emailNguoiMua = result.nguoiMua.email;
            let ten = result.nguoiMua.hoVaTen,
            dsHang = result.danhSachMua;
            mailler.sendMail('huydonhang', {ten, dsHang}, emailNguoiMua, "Đơn hàng đã bị hủy")
        }
        res.redirect('/admin/don-hang')
    } )
}