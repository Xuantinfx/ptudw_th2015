var DanhMucCap1 = require('../models/DanhMucCap1.js');
var DonHang = require('../models/DonHang');
var MatHang = require('../models/MatHang');
var NguoiDung = require('../models/NguoiDung');
var NguoiDungChuaDangNhap = require('../models/NguoiDungChuaDangNhap');
var QuanTriVien = require('../models/QuanTriVien');

var async = require('async');
exports.trangchu = function (req, res, next) {
    async.parallel([
        function(callback){
            //Lấy các thông tin tương ứng
            let query = DanhMucCap1.find({});
            query.exec(callback);
        },
        function(callback){
            //Lấy các thông tin tương ứng
            let query = MatHang.find({});
            query.exec(callback);
        }
    ], function(err, data){
        if(data != null){
            res.render('trangchu', {
                dsDanhMucCap1: req.dsDanhMucCap1,
                dsMatHang: data[1],
                user: req.user,
                tongSoLuongMatHangTrongGio: req.tongSoLuongMatHangTrongGio
            })
        }
    })
    
}