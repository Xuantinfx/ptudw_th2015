const mongoose = require("mongoose");
const QuanTriVien = require("../models/QuanTriVien");
var bcrypt = require('bcryptjs')
const ObjectId = mongoose.Types.ObjectId;

module.exports.getDsQuanTriVien = (req, res, next) => {
  try {
    QuanTriVien.find((err, results) => {
      //res.end(JSON.stringify(results));
      return res.render("admin/admin", {
        layout: "admin/layout",
        dsAdmin: results,
        user: req.user
      });
    });
  } catch (error) {
    console.log(err);
    res.redirect("/admin");
  }
};

module.exports.postThemAdmin = (req, res, next) => {
  try {
    if (req.body.email == undefined || req.body.email == "") {
      return res.end(
        JSON.stringify({
          status: false,
          message: "email không được bỏ trống"
        })
      );
    }
    if (req.body.matKhau == undefined || req.body.matKhau == "") {
      return res.end(
        JSON.stringify({
          status: false,
          message: "mật khẩu không được bỏ trống"
        })
      );
    }
    if (req.body.hoVaTen == undefined || req.body.hoVaTen == "") {
      return res.end(
        JSON.stringify({
          status: false,
          message: "họ và tên không được bỏ trống"
        })
      );
    }
    if(req.body.super == undefined || req.body.super == "") {
      return res.end(
        JSON.stringify({
          status: false,
          message: "quyền không được bỏ trống"
        })
      );
    }
    QuanTriVien.findOne({
      email: req.body.email
    }, (err, results) => {
      //res.end(JSON.stringify(results));
      //chua ton tai tai khoan
      if (results == null) {
        let temp = {
          email: req.body.email,
          matKhau: req.body.matKhau,
          hoVaTen: req.body.hoVaTen,
          super: req.body.super
        };
        //if
        let newUser = new QuanTriVien(temp);
        bcrypt.genSalt(10, function (err, salt) {
          bcrypt.hash(newUser.matKhau, salt, function (err, hash) {
            newUser.matKhau = hash;
            newUser.save();
            return res.end(
              JSON.stringify({
                status: true
              })
            );
          });
        });
      }
      //da ton tai tai khoan
      else {
        return res.end(
          JSON.stringify({
            status: false,
            message: "đã tồn tại tài khoản với email trên"
          })
        );
      }
    });
  } catch (error) {
    console.log(err);
    return res.end(
      JSON.stringify({
        status: false,
        message: "loi bat ngo"
      })
    );
  }
};

module.exports.postResetPassWordAdmin = (req, res, next) => {
  try {
    if (req.body.email == undefined || req.body.email == "") {
      return res.end(
        JSON.stringify({
          status: false,
          message: "email không hợp lệ"
        })
      );
    }
    QuanTriVien.findOne({
      email: req.body.email
    }, (err, results) => {
      //res.end(JSON.stringify(results));
      //khong ton tai tai khoan
      if (results == null) {
        return res.end(
          JSON.stringify({
            status: false,
            message: "email không tồn tại"
          })
        );
      }
      //da ton tai tai khoan
      else {
        bcrypt.genSalt(10, function (err, salt) {
          bcrypt.hash("123456", salt, function (err, hash) {
            QuanTriVien.update({
                email: req.body.email
              }, {
                $set: {
                  matKhau: hash
                }
              },
              (err, data) => {
                if (err) throw err;
              }
            );
            return res.end(JSON.stringify({
              status: true
            }));
          });
        });
      }
    });
  } catch (error) {
    console.log(err);
    return res.end(
      JSON.stringify({
        status: false,
        message: "loi bat ngo"
      })
    );
  }
};

module.exports.postXoaAdmin = (req, res, next) => {
  try {
    if (req.body.email == undefined || req.body.email == "") {
      return res.end(
        JSON.stringify({
          status: false,
          message: "email không hợp lệ"
        })
      );
    }
    if(req.user.email == req.body.email) {
      return res.end(
        JSON.stringify({
          status: false,
          message: "không được xóa chính mình"
        })
      );
    }
    QuanTriVien.findOne({
      email: req.body.email
    }, (err, results) => {
      //res.end(JSON.stringify(results));
      //khong ton tai tai khoan
      if (results == null) {
        return res.end(
          JSON.stringify({
            status: false,
            message: "email không tồn tại"
          })
        );
      }
      //da ton tai tai khoan
      else {
        QuanTriVien.findOneAndRemove({
          email: req.body.email
        }, (err, data) => {
          if (err) throw err;
          return res.end(JSON.stringify({
            status: true
          }));
        });
      }
    });
  } catch (error) {
    console.log(err);
    return res.end(
      JSON.stringify({
        status: false,
        message: "loi bat ngo"
      })
    );
  }
};

module.exports.postCapNhatAdmin = (req, res, next) => {
  try {
    if (req.body.email == undefined || req.body.email == "") {
      return res.end(
        JSON.stringify({
          status: false,
          message: "email không hợp lệ"
        })
      );
    }
    if(req.user.email == req.body.email) {
      return res.end(
        JSON.stringify({
          status: false,
          message: "không được cập nhật chính mình"
        })
      );
    }
    if (req.body.hoVaTen == undefined || req.body.hoVaTen == "") {
      return res.end(
        JSON.stringify({
          status: false,
          message: "họ và tên không được bỏ trống"
        })
      );
    }
    if (req.body.super == undefined || req.body.super == "") {
      return res.end(
        JSON.stringify({
          status: false,
          message: "super không được rỗng"
        })
      );
    }
    
    QuanTriVien.findOne({
      email: req.body.email
    }, (err, results) => {
      //res.end(JSON.stringify(results));
      //khong ton tai tai khoan
      if (results == null) {
        return res.end(
          JSON.stringify({
            status: false,
            message: "email không tồn tại"
          })
        );
      }
      //da ton tai tai khoan
      else {
        QuanTriVien.update({
            email: req.body.email
          }, {
            $set: {
              hoVaTen: req.body.hoVaTen,
              super: req.body.super
            }
          },
          (err, data) => {
            if (err) throw err;
          }
        );
        return res.end(JSON.stringify({
          status: true
        }));
      }
    });
  } catch (error) {
    console.log(err);
    return res.end(
      JSON.stringify({
        status: false,
        message: "loi bat ngo"
      })
    );
  }
};
