const mongoose = require("mongoose");
const NguoiDung = require("../models/NguoiDung");
var bcrypt = require('bcryptjs')
const ObjectId = mongoose.Types.ObjectId;

module.exports.getDsNguoiDung = (req, res, next) => {
  try {
    NguoiDung.find().populate("gioHang.idMatHang").exec((err, results) => {
      //res.end(JSON.stringify(results));
      return res.render("admin/nguoidung", {
        layout: "admin/layout",
        dsNguoiDung: results,
        user: req.user
      });
    });
  } catch (error) {
    console.log(err);
    res.redirect("/admin");
  }
};

module.exports.postThemTaiKhoan = (req, res, next) => {
  try {
    if (req.body.email == undefined || req.body.email == "") {
      return res.end(
        JSON.stringify({
          status: false,
          message: "email không được bỏ trống"
        })
      );
    }
    if (req.body.matKhau == undefined || req.body.matKhau == "") {
      return res.end(
        JSON.stringify({
          status: false,
          message: "mật khẩu không được bỏ trống"
        })
      );
    }
    if (req.body.hoVaTen == undefined || req.body.hoVaTen == "") {
      return res.end(
        JSON.stringify({
          status: false,
          message: "họ và tên không được bỏ trống"
        })
      );
    }
    NguoiDung.findOne({
      email: req.body.email
    }, (err, results) => {
      //res.end(JSON.stringify(results));
      //chua ton tai tai khoan
      if (results == null) {
        let temp = {
          email: req.body.email,
          gioHang: [],
          lichSuMuaHang: [],
          matKhau: req.body.matKhau,
          hoVaTen: req.body.hoVaTen
        };
        //if
        let newUser = new NguoiDung(temp);
        bcrypt.genSalt(10, function (err, salt) {
          bcrypt.hash(newUser.matKhau, salt, function (err, hash) {
            newUser.matKhau = hash;
            newUser.save();
            return res.end(
              JSON.stringify({
                status: true
              })
            );
          });
        });
      }
      //da ton tai tai khoan
      else {
        return res.end(
          JSON.stringify({
            status: false,
            message: "đã tồn tại tài khoản với email trên"
          })
        );
      }
    });
  } catch (error) {
    console.log(err);
    return res.end(
      JSON.stringify({
        status: false,
        message: "loi bat ngo"
      })
    );
  }
};

module.exports.postCapNhatTaiKhoan = (req, res, next) => {
  try {
    if (req.body.email == undefined || req.body.email == "") {
      return res.end(
        JSON.stringify({
          status: false,
          message: "email không hợp lệ"
        })
      );
    }
    if (req.body.hoVaTen == undefined || req.body.hoVaTen == "") {
      return res.end(
        JSON.stringify({
          status: false,
          message: "họ và tên không được bỏ trống"
        })
      );
    }
    NguoiDung.findOne({
      email: req.body.email
    }, (err, results) => {
      //res.end(JSON.stringify(results));
      //khong ton tai tai khoan
      if (results == null) {
        return res.end(
          JSON.stringify({
            status: false,
            message: "email không tồn tại"
          })
        );
      }
      //da ton tai tai khoan
      else {
        NguoiDung.update({
            email: req.body.email
          }, {
            $set: {
              hoVaTen: req.body.hoVaTen
            }
          },
          (err, data) => {
            if (err) throw err;
          }
        );
        return res.end(JSON.stringify({
          status: true
        }));
      }
    });
  } catch (error) {
    console.log(err);
    return res.end(
      JSON.stringify({
        status: false,
        message: "loi bat ngo"
      })
    );
  }
};

module.exports.postResetPassWordTaiKhoan = (req, res, next) => {
  try {
    let email = req.body.email;
    if (email == undefined || email == "") {
      return res.end(
        JSON.stringify({
          status: 'error',
          message: "email không hợp lệ"
        })
      );
    }


    let newPass = "" + Math.ceil(Math.random()*1000000000);
    NguoiDung.findOne({email: email},(err, raw) => {
        if(err) return res.end(JSON.stringify({
            status: 'error',
            message: 'Lỗi đọc dữ liệu DB'
        }))
        if(raw) {
            require('./mailler').sendMail('caplaimatkhau', {newPass: newPass, tenAdmin: req.user.hoVaTen}, email, "Cấp lại mật khẩu từ admin")
            bcrypt.genSalt(10, function (err, salt) {
                bcrypt.hash(newPass, salt, function (err, hash) {
                    newPass = hash;
                    NguoiDung.update({email: email}, {matKhau: newPass}, (err, raw2) => {
                        if(err) return res.end(JSON.stringify({
                            status: 'error',
                            message: 'Lỗi đọc dữ liệu DB'
                        }))
                        return res.end(JSON.stringify({
                            status: 'success',
                            message: 'Mật khẩu của người dùng đã được cấp mới'
                        }))
                    })
                });
            });
            
        }
        else {
          return res.end(JSON.stringify({
            status: 'error',
            message: 'Email không tồn tại'
        }))
        }
    })



    // NguoiDung.findOne({
    //   email: req.body.email
    // }, (err, results) => {
    //   //res.end(JSON.stringify(results));
    //   //khong ton tai tai khoan
    //   if (results == null) {
    //     return res.end(
    //       JSON.stringify({
    //         status: false,
    //         message: "email không tồn tại"
    //       })
    //     );
    //   }
    //   //da ton tai tai khoan
    //   else {
    //     bcrypt.genSalt(10, function (err, salt) {
    //       bcrypt.hash("123456", salt, function (err, hash) {
    //         NguoiDung.update({
    //             email: req.body.email
    //           }, {
    //             $set: {
    //               matKhau: hash
    //             }
    //           },
    //           (err, data) => {
    //             if (err) throw err;
    //           }
    //         );
    //         return res.end(JSON.stringify({
    //           status: true
    //         }));
    //       });
    //     });
    //   }
    // });
  } catch (error) {
    console.log(err);
    return res.end(
      JSON.stringify({
        status: 'error',
        message: "Lỗi bất ngờ"
      })
    );
  }
};

module.exports.postXoaTaiKhoan = (req, res, next) => {
  try {
    if (req.body.email == undefined || req.body.email == "") {
      return res.end(
        JSON.stringify({
          status: false,
          message: "email không hợp lệ"
        })
      );
    }
    NguoiDung.findOne({
      email: req.body.email
    }, (err, results) => {
      //res.end(JSON.stringify(results));
      //khong ton tai tai khoan
      if (results == null) {
        return res.end(
          JSON.stringify({
            status: false,
            message: "email không tồn tại"
          })
        );
      }
      //da ton tai tai khoan
      else {
        NguoiDung.findOneAndRemove({
          email: req.body.email
        }, (err, data) => {
          if (err) throw err;
          return res.end(JSON.stringify({
            status: true
          }));
        });
      }
    });
  } catch (error) {
    console.log(err);
    return res.end(
      JSON.stringify({
        status: false,
        message: "loi bat ngo"
      })
    );
  }
};