const NguoiDung = require('../models/NguoiDung');
const QuanTriVien = require('../models/QuanTriVien');
const MatHang = require('../models/MatHang');
const DonHang = require('../models/DonHang');
const stringShow = require("../stringShow")
const bcrypt = require('bcryptjs')
const passport = require('passport')
const mailler = require('../controller/mailler')

const trangThaiDonHang = {
    CHUA_XU_LY: "Chưa xử lí",
    DANG_GIAO: "Đang giao",
    DA_XU_LI: "Đã xử lí",
    HUY_DON: "Hủy đơn"
}

module.exports.getRegister = (req, res, next) => {
    res.render("register", {
        dsDanhMucCap1: req.dsDanhMucCap1,
        tongSoLuongMatHangTrongGio: req.tongSoLuongMatHangTrongGio
    });
}

module.exports.postRegister = (req, res, next) => {
    let email = req.body.email;
    let nhapLaiMK = req.body.rePassword;
    let matKhau = req.body.password;
    let hoVaTen = req.body.hoVaTen;

    if (!email || email == "") {
        return res.render('register', {
            error: stringShow.emptyEmail,
            ...req.body,
            dsDanhMucCap1: req.dsDanhMucCap1,
            tongSoLuongMatHangTrongGio: req.tongSoLuongMatHangTrongGio
        })
    }

    QuanTriVien.findOne({
        email: email
    }, (e, data) => {
        if (e) return res.render('register', {
            error: 'Lỗi DB',
            ...req.body,
            dsDanhMucCap1: req.dsDanhMucCap1,
            tongSoLuongMatHangTrongGio: req.tongSoLuongMatHangTrongGio
        })
        if (data) {
            return res.render('register', {
                error: stringShow.exitsEmail,
                ...req.body,
                dsDanhMucCap1: req.dsDanhMucCap1,
                tongSoLuongMatHangTrongGio: req.tongSoLuongMatHangTrongGio
            })
        } else {
            NguoiDung.findOne({
                email: email
            }, (err, result) => {
                if (err) return next(err);
                if (result) return res.render('register', {
                    error: stringShow.exitsEmail,
                    ...req.body,
                    dsDanhMucCap1: req.dsDanhMucCap1,
                    tongSoLuongMatHangTrongGio: req.tongSoLuongMatHangTrongGio
                })
                try {
                    if (!hoVaTen || hoVaTen == "") {
                        throw stringShow.emptyHoVaTen
                    }
                    if (!matKhau || matKhau == "") {
                        throw stringShow.emptyPassword
                    }
                    if (!nhapLaiMK || nhapLaiMK == "") {
                        throw stringShow.emptyRePassword
                    }
                    if (matKhau != nhapLaiMK) {
                        throw stringShow.notMatchPassword
                    }
                } catch (error) {
                    return res.render('register', {
                        error: error,
                        ...req.body,
                        dsDanhMucCap1: req.dsDanhMucCap1,
                        tongSoLuongMatHangTrongGio: req.tongSoLuongMatHangTrongGio
                    })
                }
                let activationCode = '' + Math.ceil(Math.random() * 100000000);

                mailler.sendMail('tokenactivate', {
                    urlActive: req.protocol + "://" + req.get('host') + "/active?code=" + activationCode,
                    ten: hoVaTen,
                    mail: email
                }, email, stringShow.activateAccount);

                let newUser = new NguoiDung({
                    email: email,
                    matKhau: matKhau,
                    hoVaTen: hoVaTen,
                    gioHang: [],
                    lichSuMuaHang: [],
                    activationCode: activationCode,
                });
                bcrypt.genSalt(10, function (err, salt) {
                    bcrypt.hash(newUser.matKhau, salt, function (err, hash) {
                        newUser.matKhau = hash;
                        newUser.save((err) => {
                            if (err) {
                                console.log(err)
                            }
                            req.body.username = email;
                            req.body.password = matKhau;
                            let option = {
                                successRedirect: '/chua-active',
                                failureRedirect: '/login',
                                failureFlash: true
                            }
                            passport.authenticate('local', option)(req, res, next)
                        });
                    });
                });
            })
        }
    })
}

module.exports.getChuaActive = (req, res, next) => {
    if (req.isAuthenticated()) {
        if (req.user.activationCode != '0')
            return res.render('chuaactivation', {
                user: req.user,
                dsDanhMucCap1: req.dsDanhMucCap1,
                tongSoLuongMatHangTrongGio: req.tongSoLuongMatHangTrongGio
            })
    }
    res.redirect('/')
}

//xài cho đặt hàng
module.exports.getChuaActive2 = (req, res, next) => {
    if (req.isAuthenticated()) {
        if (req.user.activationCode != '0')
            return res.render('chuaactivation', {
                user: req.user,
                dsDanhMucCap1: req.dsDanhMucCap1,
                tongSoLuongMatHangTrongGio: req.tongSoLuongMatHangTrongGio
            })
    }
    next();
}

module.exports.getActive = (req, res, next) => {
    let activationCode = req.query.code;
    if (!activationCode) {
        return res.redirect('/');
    }
    NguoiDung.find({
        activationCode: activationCode
    }, (err, result) => {
        if (err) {
            console.log(err)
            return res.redirect('/');
        }
        if (result.length > 0) {
            console.log(result)
            NguoiDung.update({
                activationCode: activationCode
            }, {
                activationCode: '0'
            }, (err, raw) => {
                console.log(err, raw)
            })
            res.render('kichhoatthanhcong', {
                user: req.user,
                dsDanhMucCap1: req.dsDanhMucCap1,
                tongSoLuongMatHangTrongGio: req.tongSoLuongMatHangTrongGio
            })
        } else {
            return res.redirect('/');
        }
    })
}

module.exports.getLogin = (req, res, next) => {
    res.render('login', {
        error: req.flash("error"),
        username: req.flash("username"),
        password: req.flash("password"),
        dsDanhMucCap1: req.dsDanhMucCap1,
        tongSoLuongMatHangTrongGio: req.tongSoLuongMatHangTrongGio
    });
}


//module.exports.postLogin = passport.authenticate('local', option)

module.exports.postLogin = (req, res, next) => {
    let nextUrl = req.query.nextUrl;
    let option = {
        successRedirect: nextUrl != "" && nextUrl != undefined ? nextUrl : '/',
        failureRedirect: '/login',
        failureFlash: true
    }
    passport.authenticate('local', option)(req, res, next)
}

module.exports.postLogout = (req, res, next) => {
    if (req.logout) {
        req.logout()
    }
    res.redirect('/');
}

module.exports.getQuenMatKhau = (req, res, next) => {
    res.render('quenmatkhau', {
        dsDanhMucCap1: req.dsDanhMucCap1,
        tongSoLuongMatHangTrongGio: req.tongSoLuongMatHangTrongGio
    })
}

module.exports.postQuenMatKhau = (req, res, next) => {
    console.log(req.body);
    let email = req.body.email;
    if (email == undefined || !email) {
        return res.end(JSON.stringify({
            status: 'error',
            message: 'Không được bỏ trống email',
            dsDanhMucCap1: req.dsDanhMucCap1,
            tongSoLuongMatHangTrongGio: req.tongSoLuongMatHangTrongGio
        }))
    }
    let newPass = "" + Math.ceil(Math.random() * 1000000000);
    NguoiDung.find({
        email: email
    }, (err, raw) => {
        if (err) return res.end(JSON.stringify({
            status: 'error',
            message: 'Lỗi đọc dữ liệu DB'
        }))
        if (raw.length > 0) {
            require('./mailler').sendMail('quenmatkhau', {
                newPass: newPass
            }, email, "Yêu cầu cấp mới mật khẩu")
            bcrypt.genSalt(10, function (err, salt) {
                bcrypt.hash(newPass, salt, function (err, hash) {
                    newPass = hash;
                    NguoiDung.update({
                        email: email
                    }, {
                        matKhau: newPass
                    }, (err, raw2) => {
                        if (err) return res.end(JSON.stringify({
                            status: 'error',
                            message: 'Lỗi đọc dữ liệu DB'
                        }))
                        return res.end(JSON.stringify({
                            status: 'success',
                            message: 'Vui lòng kiểm tra email để lấy mật khẩu mới'
                        }))
                    })
                });
            });

        } else {
            //có thể là admin
            QuanTriVien.find({
                email: email
            }, (err, raw) => {
                if (err) return res.end(JSON.stringify({
                    status: 'error',
                    message: 'Lỗi đọc dữ liệu DB'
                }))
                if (raw.length > 0) {
                    require('./mailler').sendMail('quenmatkhau', {
                        newPass: newPass
                    }, email, "Yêu cầu cấp mới mật khẩu")
                    bcrypt.genSalt(10, function (err, salt) {
                        bcrypt.hash(newPass, salt, function (err, hash) {
                            newPass = hash;
                            QuanTriVien.update({
                                email: email
                            }, {
                                matKhau: newPass
                            }, (err, raw2) => {
                                if (err) return res.end(JSON.stringify({
                                    status: 'error',
                                    message: 'Lỗi đọc dữ liệu DB'
                                }))
                                return res.end(JSON.stringify({
                                    status: 'success',
                                    message: 'Vui lòng kiểm tra email để lấy mật khẩu mới'
                                }))
                            })
                        });
                    });

                } else {
                    return res.end(JSON.stringify({
                        status: 'error',
                        message: 'Email không tồn tại'
                    }))
                }
            })
        }
    })

}

module.exports.getThongTinCaNhan = (req, res, next) => {
    res.render('thongtincanhan', {
        user: req.user,
        dsDanhMucCap1: req.dsDanhMucCap1,
        tongSoLuongMatHangTrongGio: req.tongSoLuongMatHangTrongGio
    });
}

module.exports.postDoiTen = (req, res, next) => {
    let hoVaTen = req.body.hoVaTen;
    if (hoVaTen == undefined || hoVaTen == '') {
        res.end(JSON.stringify({
            status: "error",
            message: "Họ và tên không được rỗng"
        }))
    }

    NguoiDung.update({
        email: req.user.email
    }, {
        hoVaTen: hoVaTen
    }, (err, raw) => {
        if (err) {
            res.end(JSON.stringify({
                status: "error",
                message: "Lỗi đọc DB"
            }))
        }

        res.end(JSON.stringify({
            status: "success",
            message: "Họ và tên đã được đổi thành " + hoVaTen
        }))

    })
}

module.exports.postDoiMatKhau = (req, res, next) => {
    let matKhauCu = req.body.matKhauCu;
    let matKhauMoi = req.body.matKhauMoi;
    let reMatKhauMoi = req.body.reMatKhauMoi;

    if (matKhauCu == undefined || matKhauCu == '') {
        res.end(JSON.stringify({
            status: "error",
            message: "Mật khẩu cũ không được rỗng"
        }))
    }
    if (matKhauMoi == undefined || matKhauMoi == '') {
        res.end(JSON.stringify({
            status: "error",
            message: "Mật khẩu mới không được rỗng"
        }))
    }
    if (reMatKhauMoi == undefined || reMatKhauMoi == '') {
        res.end(JSON.stringify({
            status: "error",
            message: "Nhập lại mật khẩu mới không được rỗng"
        }))
    }

    bcrypt.compare(matKhauCu, req.user.matKhau, function (err, isMatch) {
        if (err) return res.end(JSON.stringify({
            status: "error",
            message: "Lỗi so sánh mật khẩu"
        }))
        if (isMatch) {
            if (matKhauMoi != reMatKhauMoi) {
                return res.end(JSON.stringify({
                    status: "error",
                    message: "Mật khẩu mới và nhập lại mật khẩu mới không khớp"
                }))
            }
            bcrypt.genSalt(10, function (err, salt) {
                bcrypt.hash(matKhauMoi, salt, function (err, hash) {
                    matKhauMoi = hash;
                    NguoiDung.update({
                        email: req.user.email
                    }, {
                        matKhau: matKhauMoi
                    }, (err, raw) => {
                        if (err) {
                            return res.end(JSON.stringify({
                                status: "error",
                                message: "Lỗi cập nhật mật khẩu"
                            }))
                        }
                        return res.end(JSON.stringify({
                            status: "success",
                            message: "Mật khẩu đã được thay đổi"
                        }))
                    })
                });
            });
        } else return res.end(JSON.stringify({
            status: "error",
            message: "Sai mật khẩu cũ"
        }))
    });
}

module.exports.choSanPhamVaoGioHang = function (req, res) {
    // console.log(req.userChuaDangNhap.gioHang);
    // return res.redirect('back');
    let gioHang = null;
    if (req.user) {
        gioHang = req.user.gioHang;
    } else gioHang = req.userChuaDangNhap.gioHang;
    switch (req.query.action) {
        case "them":
            //kiểm tra trùng
            let flagTrung = false;
            for (let i = 0; i < gioHang.length; i++) {
                if (gioHang[i].maSo == req.body.maso) {
                    console.log('tim thay trung');
                    gioHang[i].soLuong += parseInt(req.body.soluong);
                    flagTrung = true;
                    //update database
                    if (req.user) {
                        NguoiDung.findOne({
                            email: req.user.email
                        }, (err, result) => {
                            if (err) throw err;
                            else {
                                result.gioHang = gioHang;
                                result.save((err) => {
                                    if (err) console.log('Save loi:', err);
                                    res.redirect('back');
                                })
                            }
                        })
                    } else { //trường hợp người dùng chưa đăng nhập
                        req.userChuaDangNhap.save((err) => {
                            if (err) console.log('Save loi:', err);
                            res.redirect('back');
                        })
                    }
                    break;
                }
            }
            //nếu không trùng thì thêm mới vào
            if (!flagTrung) {
                MatHang.findOne({
                    maSo: req.body.maso
                }, (err, result) => {
                    if (err) {
                        console.log(err);
                        res.redirect('back');
                    } else {
                        if (result) {
                            let object = {
                                idMatHang: result._id,
                                maSo: req.body.maso,
                                soLuong: parseInt(req.body.soluong)
                            }
                            gioHang.push(object);
                            //update database
                            if (req.user) {
                                NguoiDung.findOne({
                                    email: req.user.email
                                }, (err, result) => {
                                    if (err) throw err;
                                    else {
                                        result.gioHang = gioHang;
                                        result.save((err) => {
                                            if (err) console.log('Save loi:', err);
                                            res.redirect('back');
                                        })

                                    }
                                })
                            } else { //trường hợp người dùng chưa đăng nhập
                                req.userChuaDangNhap.save((err) => {
                                    if (err) console.log('Save loi:', err);
                                    res.redirect('back');
                                })
                            }

                        } else {
                            console.log(`Khong tim duoc mat hang tuong ung: ${req.body.maso}`);
                            res.redirect('back');
                        }
                    }
                })
            }
            break;
        case "bot":
            for (let i = 0; i < gioHang.length; i++) {
                if (gioHang[i].maSo == req.body.maso && gioHang[i].soLuong > 1) {
                    gioHang[i].soLuong -= parseInt(req.body.soluong);
                    //update database
                    if (req.user) {
                        NguoiDung.findOne({
                            email: req.user.email
                        }, (err, result) => {
                            if (err) throw err;
                            else {
                                result.gioHang = gioHang;
                                result.save((err) => {
                                    if (err) console.log('Save loi:', err);
                                    res.redirect('back');
                                })
                            }
                        })
                    } else { //trường hợp người dùng chưa đăng nhập
                        req.userChuaDangNhap.save((err) => {
                            if (err) console.log('Save loi:', err);
                            res.redirect('back');
                        })
                    }
                    return;
                }
            }
            res.redirect('back');
            break;
        case "xoa":
            for (let i = 0; i < gioHang.length; i++) {
                if (gioHang[i].maSo == req.body.maso) {
                    console.log('tim thay trung');
                    gioHang.splice(i, 1);
                    //update database
                    if (req.user) {
                        NguoiDung.findOne({
                            email: req.user.email
                        }, (err, result) => {
                            if (err) throw err;
                            else {
                                result.gioHang = gioHang;
                                result.save((err) => {
                                    if (err) console.log('Save loi:', err);
                                    res.redirect('back');
                                })

                            }
                        })
                    } else { //trường hợp người dùng chưa đăng nhập
                        req.userChuaDangNhap.save((err) => {
                            if (err) console.log('Save loi:', err);
                            res.redirect('back');
                        })
                    }
                    break;
                }
            }
            break;
        default:
            console.log(`Action khong hop le: ${req.query.action}`);
            res.redirect('back');
    }
}


module.exports.getGioHang = function (req, res) {
    let gioHang = null;
    let tongTien = 0;

    if (req.user) {
        gioHang = req.user.gioHang;
    } else { //nếu là người dùng chưa đăng nhập
        gioHang = req.userChuaDangNhap.gioHang;
    }
    if (gioHang.length == 0) res.render('giohang', {
        gioHang: gioHang,
        tongTien: tongTien,
        user: req.user,
        dsDanhMucCap1: req.dsDanhMucCap1,
        tongSoLuongMatHangTrongGio: req.tongSoLuongMatHangTrongGio
    })
    let doneFlag = 0;
    for (let i = 0; i < gioHang.length; i++) {
        MatHang.findById(gioHang[i].idMatHang, 'ten gia', (err, result) => {
            if (err) {
                console.log(err);
            } else {
                gioHang[i].ten = result.ten;
                gioHang[i].donGia = result.gia;
                gioHang[i].thanhTien = parseInt(gioHang[i].soLuong) * parseInt(result.gia);
                tongTien += gioHang[i].thanhTien;
                doneFlag++;
            }
            //chỉ khi nào thực hiện xong hết mới render
            if (doneFlag == gioHang.length) {
                res.render('giohang', {
                    gioHang: gioHang,
                    tongTien: tongTien,
                    user: req.user,
                    dsDanhMucCap1: req.dsDanhMucCap1,
                    tongSoLuongMatHangTrongGio: req.tongSoLuongMatHangTrongGio
                })
            }
        })
    }
}

module.exports.datHang = function (req, res) {
    let donHang_new = {
        nguoiMua: req.user._id,
        diaChi: req.body.diachi,
        danhSachMua: [],
        tongTien: 0,
        moTaTrangThai: req.body.ghichu,
        ngayGiao: req.body.ngaygiao
    }
    let gioHang = req.user.gioHang;
    if (gioHang.length == 0) return res.redirect('back');
    //tạo danh sách mua từ giỏ hàng
    let flagDone = 0;
    for (let i = 0; i < gioHang.length; i++) {
        let matHangMua = {
            idMatHang: gioHang[i].idMatHang,
            soLuong: gioHang[i].soLuong,
        }
        //lấy giá
        MatHang.findById(gioHang[i].idMatHang, (err, result) => {
            if (err) console.log(err);
            else {
                matHangMua.gia = result.gia;
                matHangMua.thanhTien = result.gia * matHangMua.soLuong;
                donHang_new.tongTien += matHangMua.thanhTien;
                donHang_new.danhSachMua.push(matHangMua);
                flagDone++;
                if (flagDone == gioHang.length) {
                    DonHang.create(donHang_new, (err, result) => { //tạo đơn hàng mới
                        if (err) {
                            console.log(err);
                            res.redirect('back');
                        } else { //clear giỏ hàng cũ và thêm id đơn hàng váo lịch sử mua
                            ClearGioHang(req, res, result._id);
                        }
                    })
                }
            }
        })
    }
}

ClearGioHang = function (req, res, idDonHang_new) {
    console.log('vào clear')
    NguoiDung.findOne({
        email: req.user.email
    }, (err, result) => {
        if (err) throw err;
        else {
            console.log('vào save')
            result.gioHang = [];
            result.lichSuMuaHang.push({
                idDonHang: idDonHang_new
            });
            result.save((err) => {
                if (err) console.log('Save loi:', err);
                res.redirect('back');
            })
        }
    })
}

module.exports.getLichSuMuaHang = function (req, res) {
    NguoiDung.findById(req.user._id).populate({
        path: "lichSuMuaHang.idDonHang",
        populate: {
            path: "danhSachMua.idMatHang"
        }
    }).exec((err, result) => {
        if (err) {
            res.redirect("/");
            return;
        }
        let lichSuMuaHang = result.lichSuMuaHang;
        let dsDonHangChoXuLi = [],
            dsDonHangDangGiao = [],
            dsDonHangDaXuLi = [];
        for (let i = 0; i < lichSuMuaHang.length; i++) {
            switch (lichSuMuaHang[i].idDonHang.trangThai) {
                case trangThaiDonHang.CHUA_XU_LY:
                    dsDonHangChoXuLi.push(lichSuMuaHang[i].idDonHang)
                    break;
                case trangThaiDonHang.DANG_GIAO:
                    dsDonHangDangGiao.push(lichSuMuaHang[i].idDonHang)
                    break;
                case trangThaiDonHang.DA_XU_LI:
                    dsDonHangDaXuLi.push(lichSuMuaHang[i].idDonHang)
                    break;
                case trangThaiDonHang.HUY_DON:
                    dsDonHangDaXuLi.push(lichSuMuaHang[i].idDonHang)
                    break;
            }
        }
        res.render('lichsumuahang', {
            dsDonHangChoXuLi,
            dsDonHangDangGiao,
            dsDonHangDaXuLi,
            user: req.user,
            dsDanhMucCap1: req.dsDanhMucCap1,
            tongSoLuongMatHangTrongGio: req.tongSoLuongMatHangTrongGio
        })
    })
}

module.exports.getHuyDonHang = (req, res, next) => {
    let id = req.query.id;
    if (!id) {
        res.redirect('/lich-su-mua-hang')
        return;
    }
    DonHang.findByIdAndUpdate(id, {
            trangThai: trangThaiDonHang.HUY_DON
        }, {
            upsert: false
        })
        .populate("nguoiMua")
        .populate("danhSachMua.idMatHang")
        .exec((err, result) => {
            if (!err) {
                let emailNguoiMua = result.nguoiMua.email;
                let ten = result.nguoiMua.hoVaTen,
                    dsHang = result.danhSachMua;
                mailler.sendMail('nguoidunghuydonhang', {
                    ten,
                    dsHang
                }, emailNguoiMua, "Đơn hàng đã được hủy")
            }
            res.redirect('/lich-su-mua-hang')
        })
}