//thêm các model
var DanhMucCap1 = require('../models/DanhMucCap1.js');
var DonHang = require('../models/DonHang');
var MatHang = require('../models/MatHang');
var NguoiDung = require('../models/NguoiDung');
var NguoiDungChuaDangNhap = require('../models/NguoiDungChuaDangNhap');
var QuanTriVien = require('../models/QuanTriVien');

//mỗi handler sẽ được controller export ra

//xem danh sách sản phẩm chung: render ra trang /admin/san-pham
exports.xemDanhSachSanPham = function (req, res, next) {
    //lấy {maso, ten, giá, khối lượng tịnh, số lượng sp con}
    MatHang.find({}, 'maSo ten gia khoiLuongTinh soLuongSPTrongMatHang', (err, data) => {
        if (err) throw err;
        res.render('admin/sanpham', {
            layout: 'admin/layout',
            dsSanPham: data,
            user: req.user
        });
    })
}

//xem chi tiết sản phẩm: (id) => {đầy đủ các thuộc tính} => trả về chuỗi HTML để GUI thêm vào modal-body
exports.xemSanPhamChiTiet = function (req, res, next) {
    console.log("Vào controller: ", req.params.idSanPhamXemChiTiet)
    //lấy {maSo, ten, gia, khoiLuongTinh, soLuongSPTrongMatHang, tinhTrang, moTa, thongTinChiTiet}
    MatHang.findOne({
        'maSo': `${req.params.idSanPhamXemChiTiet}`
    }, 'maSo ten gia khoiLuongTinh soLuongSPTrongMatHang tinhTrang moTa thongTinChiTiet', (err, data) => {
        if (err) throw err;
        else {
            let chuoiKetQuaXemChiTiet = `
            <form class=form-row>
                <div class="form-group col-md-12">
                    <label for="maSo" class="col-form-label font-weight-bold">Mã số</label>
                    <input type="text" class="form-control" id="maSo" value="${data.maSo}" disabled>
                </div>
                <div class="form-group col-md-12">
                    <label for="ten" class="col-form-label font-weight-bold">Tên</label>
                    <input type="text" class="form-control" id="ten" value="${data.ten}" disabled>
                </div>
                <div class="form-group col-md-6">
                    <label for="gia" class="col-form-label font-weight-bold">Giá</label>
                    <input type="text" class="form-control" id="gia" value="${data.gia}" disabled>
                </div>
                <div class="form-group col-md-6">
                    <label for="khoiLuongTinh" class="col-form-label font-weight-bold">Khối lượng tịnh</label>
                    <input type="text" class="form-control" id="khoiLuongTinh" value="${data.khoiLuongTinh}" disabled>
                </div>
                <div class="form-group col-md-6">
                    <label for="soLuongSPCon" class="col-form-label font-weight-bold">Số lượng sản phẩm trong mặt hàng</label>
                    <input type="text" class="form-control" id="soLuongSPTrongMatHang" value="${data.soLuongSPTrongMatHang}" disabled>
                </div>
                <div class="form-group col-md-6">
                    <label for="tinhTrang" class="col-form-label font-weight-bold">Tình trạng</label>
                    <input type="text" class="form-control" id="tinhTrang" value="${data.tinhTrang}" disabled>
                </div>
                <div class="form-group col-md-12">
                    <label for="moTa" class="col-form-label font-weight-bold">Mô tả</label>
                    <textarea type="text" class="form-control" id="moTa" disabled>${data.moTa}</textarea>
                </div>
                <div class="form-group col-md-12">
                    <label for="thongTinChiTiet" class="col-form-label font-weight-bold">Thông tin chi tiết</label>
                    <div class="form-group">
                        <label for="thanhPhan" class="col-form-label">Thành phần</label>
                        <textarea type="text" class="form-control" id="thanhPhan" disabled>${data.thongTinChiTiet.thanhPhan}</textarea>
                        
                        <label for="huongDanSuDung" class="col-form-label">Hướng dẫn sử dụng</label>
                        <textarea type="text" class="form-control" id="huongDanSuDung" disabled>${data.thongTinChiTiet.huongDanSuDung}</textarea>
                        
                        <label for="baoQuan" class="col-form-label">Bảo quản</label>
                        <textarea type="text" class="form-control" id="baoQuan" disabled>${data.thongTinChiTiet.baoQuan}</textarea>
                    </div>
                </div>
            </form>`
            res.end(chuoiKetQuaXemChiTiet);
        }
    })
}


//xóa sản phẩm: (id) => true/false
exports.xoaSanPham = function (req, res, next) {
    let idSanPhamXoa = req.params.idSanPhamXoa;
    console.log(idSanPhamXoa);
    MatHang.findOneAndRemove({
        maSo: idSanPhamXoa
    }, (err, data) => {
        if (err) {
            throw err;
            res.end('false');
        } else {
            console.log(data);
            if (data == null) res.end('false');
            else {
                res.end('true');
                //tìm và xóa trong danh mục cấp 1, 2, thương hiệu
                //chưa làm, cẩn sửa database, thêm thuộc tính danh mục 1, danh mục 2, thương hiệu cho sản phẩm
            }
        }
    })
}

//thêm sản phẩm: ({đầy đủ các thuộc tính}) => true/false
//URL /admin/san-pham/them?idDanhMucCap1=&idDanhMucCap2=&idThuongHieu=&data
exports.themSanPham = function (req, res, next) {
    let idDanhMucCap1 = req.query.idDanhMucCap1;
    let idDanhMucCap2 = req.query.idDanhMucCap2;
    let idThuongHieu = req.query.idThuongHieu;
    let data = JSON.parse(req.query.data);

    MatHang.create(data, (err, data) => {
        if (err) {
            throw err;
            res.end('');
        } else {
            if (data == null) res.end('');
            else {
                //thêm idMatHang vào danh mục cấp 1, 2, thương hiệu phù hợp
                let idMatHang = data._id;
                DanhMucCap1.findOne({
                    maSo: idDanhMucCap1
                }, (err, data) => {
                    if (err) throw err;
                    else {
                        for (let i = 0; i < data.danhMucCap2.length; i++) {
                            if (data.danhMucCap2[i].maSo == idDanhMucCap2) {
                                for (let j = 0; j < data.danhMucCap2[i].thuongHieu.length; j++) {
                                    if (data.danhMucCap2[i].thuongHieu[j].maSo == idThuongHieu) data.danhMucCap2[i].thuongHieu[j].idMatHang.push(idMatHang);
                                }
                            }
                        }
                        data.save((err) => {
                            if (err) res.end('false');
                            else res.end('true');
                        })
                    }
                })

            }
        }
    })


}
//cache danhMucCap2
let dsDanhMucCap2;
// load danh mục cấp 1
exports.layDsDanhMucCap1 = function (req, res, next) {
    DanhMucCap1.find({}, 'ten maSo', (err, data) => {
        if (err) {
            throw err;
            res.end('');
        } else {
            if (data == null) res.end('');
            else {
                let chuoiGiaoDien = '';
                for (let i = 0; i < data.length; i++) {
                    chuoiGiaoDien += `<option data-id="${data[i].maSo}">${data[i].ten}</option>`
                }
                res.end(chuoiGiaoDien);
            }
        }
    })
}
//load danh mục cấp 2
exports.layDsDanhMucCap2 = function (req, res, next) {
    DanhMucCap1.findOne({
        maSo: `${req.query.idDanhMucCap1}`
    }, 'danhMucCap2', (err, data) => {
        if (err) {
            throw err;
            res.end('');
        } else {
            if (data == null) res.end('');
            else {
                let chuoiGiaoDien = '';
                dsDanhMucCap2 = data.danhMucCap2;
                for (let i = 0; i < data.danhMucCap2.length; i++) {
                    chuoiGiaoDien += `<option data-id='${data.danhMucCap2[i].maSo}'>${data.danhMucCap2[i].ten}</option>`
                }
                res.end(chuoiGiaoDien);
            }
        }
    })
}
//load thương hiệu trong danh mục cấp 2
exports.layDsThuongHieu = function (req, res, next) {
    // console.log('Vào controller', dsDanhMucCap2);
    let chuoiGiaoDien = ''
    for (let i = 0; i < dsDanhMucCap2.length; i++) {
        if (dsDanhMucCap2[i].maSo == req.query.idDanhMucCap2) {
            for (let j = 0; j < dsDanhMucCap2[i].thuongHieu.length; j++) {
                chuoiGiaoDien += `<option data-id='${dsDanhMucCap2[i].thuongHieu[j].maSo}'>${dsDanhMucCap2[i].thuongHieu[j].ten}</option>`
            }
            break;
        }
    }
    res.end(chuoiGiaoDien);

}

//cập nhật sản phẩm ({đầy đủ các thuộc tính}) => true/false
exports.capNhatSanPham = function (req, res, next) {
    dataCapNhat = JSON.parse(req.query.data);
    //thực hiện query update
    MatHang.update({
        'maSo': req.params.idSanPhamCapNhat
    }, {
        $set: dataCapNhat
    }, (err, data) => {
        if (err) {
            throw err;
            res.end("false");
        } else {
            res.end("true");
        }
    })
}
//xem trước khi cập nhật sản phẩm
exports.xemTruocKhiCapNhatSanPham = function (req, res, next) {
    console.log("Vào controller: ", req.params.idSanPhamCapNhat)
    //lấy {maSo, ten, gia, khoiLuongTinh, soLuongSPTrongMatHang, tinhTrang, moTa, thongTinChiTiet}
    MatHang.findOne({
        'maSo': `${req.params.idSanPhamCapNhat}`
    }, 'maSo ten gia khoiLuongTinh soLuongSPTrongMatHang tinhTrang moTa thongTinChiTiet', (err, data) => {
        if (err) throw err;
        else {
            let chuoiKetQuaXemChiTiet = `
            <form class="form-row">
                <div class="form-group col-md-12">
                    <label for="maSo" class="col-form-label font-weight-bold">Mã số</label>
                    <input type="text" class="form-control" id="maSo" value="${data.maSo}" disabled>
                </div>
                <div class="form-group col-md-12">
                    <label for="ten" class="col-form-label font-weight-bold">Tên</label>
                    <input type="text" class="form-control" id="ten" value="${data.ten}">
                </div>
                <div class="form-group col-md-6">
                    <label for="gia" class="col-form-label font-weight-bold">Giá</label>
                    <input type="text" class="form-control" id="gia" value="${data.gia}">
                </div>
                <div class="form-group col-md-6">
                    <label for="khoiLuongTinh" class="col-form-label font-weight-bold">Khối lượng tịnh</label>
                    <input type="text" class="form-control" id="khoiLuongTinh" value="${data.khoiLuongTinh}">
                </div>
                <div class="form-group col-md-6">
                    <label for="soLuongSPCon" class="col-form-label font-weight-bold">Số lượng sản phẩm trong mặt hàng</label>
                    <input type="text" class="form-control" id="soLuongSPTrongMatHang" value="${data.soLuongSPTrongMatHang}">
                </div>
                <div class="form-group col-md-6">
                    <label for="tinhTrang" class="col-form-label font-weight-bold">Tình trạng</label>
                    <input type="text" class="form-control" id="tinhTrang" value="${data.tinhTrang}">
                </div>
                <div class="form-group col-md-12">
                    <label for="moTa" class="col-form-label font-weight-bold">Mô tả</label>
                    <textarea type="text" class="form-control" id="moTa">${data.moTa}</textarea>
                </div>
                <div class="form-group col-md-12">
                    <label for="thongTinChiTiet" class="col-form-label font-weight-bold">Thông tin chi tiết</label>
                    <div class="form-group">
                        <label for="thanhPhan" class="col-form-label">Thành phần</label>
                        <textarea type="text" class="form-control" id="thanhPhan">${data.thongTinChiTiet.thanhPhan}</textarea>
                        
                        <label for="huongDanSuDung" class="col-form-label">Hướng dẫn sử dụng</label>
                        <textarea type="text" class="form-control" id="huongDanSuDung">${data.thongTinChiTiet.huongDanSuDung}</textarea>
                        
                        <label for="baoQuan" class="col-form-label">Bảo quản</label>
                        <textarea type="text" class="form-control" id="baoQuan">${data.thongTinChiTiet.baoQuan}</textarea>
                    </div>
                </div>
            </form>`
            res.end(chuoiKetQuaXemChiTiet);
        }
    })
}