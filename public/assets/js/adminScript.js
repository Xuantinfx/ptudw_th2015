/*
    Yêu cầu xem chi tiết sản phẩm
*/
$('#modalXemChiTiet').on('show.bs.modal', function (event) {
    let button = $(event.relatedTarget) // Button that triggered the modal\
    let idSanPhamXemChiTiet = button.data('id');
    $.get(`/admin/san-pham/xem-chi-tiet/${idSanPhamXemChiTiet}`, (data, textStatus) => {
        let modal = $(this);
        modal.find('.modal-body').html(data);
    })
})

/*
    Cập nhật thông tin sản phẩm
*/
//gửi yêu cầu xem trước khi cập nhật
$('#modalCapNhatSanPham').on('show.bs.modal', function (event) {
    let button = $(event.relatedTarget) // Button that triggered the modal
    let idSanPhamXemChiTiet = button.data('id');
    $.get(`/admin/san-pham/xem-truoc-khi-cap-nhat/${idSanPhamXemChiTiet}`, (data, textStatus) => {
        let modal = $(this);
        modal.find('.modal-body').html(data);
    })
})
//xác nhận cập nhật
$('#btnXacNhanCapNhat').click(function () {
    let idSanPhamCapNhat = $('#modalCapNhatSanPham').find('#maSo').val();
    //kiểm tra số lượng SP con và giá
    if (parseInt($('#modalCapNhatSanPham').find('#soLuongSPTrongMatHang').val()) <= 0 || parseInt($('#modalCapNhatSanPham').find('#gia').val()) <= 0) {
        alert("Kiểm tra lại giá và số lượng sản phẩm trong mặt hàng");
        return;
    }
    //Object gửi về cập nhật
    let thongTinCapNhat = {
        thongTinChiTiet: {
            thanhPhan: $('#modalCapNhatSanPham').find('#thanhPhan').val(),
            huongDanSuDung: $('#modalCapNhatSanPham').find('#huongDanSuDung').val(),
            baoQuan: $('#modalCapNhatSanPham').find('#baoQuan').val()
        },
        soLuongSPTrongMatHang: parseInt($('#modalCapNhatSanPham').find('#soLuongSPTrongMatHang').val()),
        tinhTrang: $('#modalCapNhatSanPham').find('#tinhTrang').val(),
        moTa: $('#modalCapNhatSanPham').find('#moTa').val(),
        ten: $('#modalCapNhatSanPham').find('#ten').val(),
        khoiLuongTinh: $('#modalCapNhatSanPham').find('#khoiLuongTinh').val(),
        gia: parseInt($('#modalCapNhatSanPham').find('#gia').val())
    }
    $.post(`/admin/san-pham/cap-nhat/${idSanPhamCapNhat}?data=${encodeURIComponent(JSON.stringify(thongTinCapNhat))}`, (data, textStatus) => {
        if (data == "true") {
            $('#modalCapNhatSanPham').modal('hide');
            alert('Cập nhật thành công');
            //cập nhật lại DOM chỗ table
            $(`#${idSanPhamCapNhat}`).find('td')[1].innerText = $('#modalCapNhatSanPham').find('#ten').val() //tên
            $(`#${idSanPhamCapNhat}`).find('td')[2].innerText = $('#modalCapNhatSanPham').find('#gia').val() //giá
            $(`#${idSanPhamCapNhat}`).find('td')[3].innerText = $('#modalCapNhatSanPham').find('#khoiLuongTinh').val() //giá
            $(`#${idSanPhamCapNhat}`).find('td')[4].innerText = $('#modalCapNhatSanPham').find('#soLuongSPTrongMatHang').val() //giá
        } else {
            $('#modalCapNhatSanPham').modal('hide');
            alert('Cập nhật thất bại')
        }
    });
    console.log(thongTinCapNhat);

})

/*
    Các hàm phục vụ cho việc load danh mục 1, 2, thương hiệu trong phần thêm sản phẩm
 */
//khi các danh mục 1, danh mục 2, thương hiệu được load
$(".dsDanhMucCap1").ready(function () {
    $.get(`/admin/san-pham/ds-danh-muc-cap-1`, (data, textStatus) => {
        $('.dsDanhMucCap1').html(data);
        //load tiếp danh mục cấp 2
        $.get(`/admin/san-pham/ds-danh-muc-cap-2?idDanhMucCap1=` + $('#modalThemSanPham .dsDanhMucCap1 option:selected').data('id'), (data, textStatus) => {
            $('.dsDanhMucCap2').html(data);
            //load tiếp danh sách thương hiệu urgh
            $.get(`/admin/san-pham/ds-thuong-hieu?idDanhMucCap2=` + $('#modalThemSanPham .dsDanhMucCap2 option:selected').data('id'), (data, textStatus) => {
                $('.dsThuongHieu').html(data);
            })
        })
    })
});
//danh mục 1 change thì load danh mục cấp 2 và thương hiệu
$(".dsDanhMucCap1").change(function () {
    $.get(`/admin/san-pham/ds-danh-muc-cap-2?idDanhMucCap1=` + $('#modalThemSanPham .dsDanhMucCap1 option:selected').data('id'), (data, textStatus) => {
        $('.dsDanhMucCap2').html(data);
        //load tiếp danh sách thương hiệu urgh
        $.get(`/admin/san-pham/ds-thuong-hieu?idDanhMucCap2=` + $('#modalThemSanPham .dsDanhMucCap2 option:selected').data('id'), (data, textStatus) => {
            $('.dsThuongHieu').html(data);
        })
    })
})
//danh mục 2 change thì load lại thương hiệu
$(".dsDanhMucCap2").change(function () {
    $.get(`/admin/san-pham/ds-thuong-hieu?idDanhMucCap2=` + $('#modalThemSanPham .dsDanhMucCap2 option:selected').data('id'), (data, textStatus) => {
        $('.dsThuongHieu').html(data);
    })
})

/*
    Thêm sản phẩm mới
*/
//hiển thị form thêm
$('#modalThemSanPham').on('show.bs.modal', function (event) {
    //xóa các nội dung cũ
    $(this).find('input').val('');
    $(this).find('textarea').val('');
})
//xác nhận thêm sản phẩm
$('#btnXacNhanThem').click(function () {
    //kiểm tra số lượng SP con và giá
    if (parseInt($('#modalThemSanPham').find('#soLuongSPTrongMatHang').val()) <= 0 || parseInt($('#modalThemSanPham').find('#gia').val()) <= 0) {
        alert("Kiểm tra lại giá và số lượng sản phẩm trong mặt hàng");
        return;
    }
    //kiểm tra tên
    if ($('#modalThemSanPham').find('#ten').val() == '' || $('#modalThemSanPham').find('#ten').val() == null) {
        alert("Kiểm tra lại tên sản phẩm");
        return;
    }
    //tạo mã số
    let maSo = $('#modalThemSanPham').find('#ten').val().toLowerCase().trim().replace(/ /g, '-');
    //Object gửi về cập nhật
    let thongTinCapNhat = {
        maSo: maSo,
        thongTinChiTiet: {
            thanhPhan: $('#modalThemSanPham').find('#thanhPhan').val(),
            huongDanSuDung: $('#modalThemSanPham').find('#huongDanSuDung').val(),
            baoQuan: $('#modalThemSanPham').find('#baoQuan').val()
        },
        soLuongSPTrongMatHang: parseInt($('#modalThemSanPham').find('#soLuongSPTrongMatHang').val()),
        tinhTrang: $('#modalThemSanPham').find('#tinhTrang').val(),
        moTa: $('#modalThemSanPham').find('#moTa').val(),
        ten: $('#modalThemSanPham').find('#ten').val(),
        khoiLuongTinh: $('#modalThemSanPham').find('#khoiLuongTinh').val(),
        gia: parseInt($('#modalThemSanPham').find('#gia').val())
    }
    // URL /admin/san-pham/them?idDanhMucCap1=&idDanhMucCap2=&idThuongHieu=&data
    let idDanhMucCap1 = $('#modalThemSanPham .dsDanhMucCap1 option:selected').data('id');
    let idDanhMucCap2 = $('#modalThemSanPham .dsDanhMucCap2 option:selected').data('id');
    let idThuongHieu = $('#modalThemSanPham .dsThuongHieu option:selected').data('id');
    $.post(`/admin/san-pham/them?idDanhMucCap1=${idDanhMucCap1}&idDanhMucCap2=${idDanhMucCap2}&idThuongHieu=${idThuongHieu}&data=${encodeURIComponent(JSON.stringify(thongTinCapNhat))}`, (data, textStatus) => {
        if (data == "true") {
            $('#modalThemSanPham').modal('hide');
            alert('Cập nhật thành công');
            //cập nhật lại DOM chỗ table
            //chưa cập nhật
        } else {
            $('#modalThemSanPham').modal('hide');
            alert('Cập nhật thất bại')
        }
    });
    console.log(thongTinCapNhat);

})

/*
    Xóa sản phẩm
*/
//hiển thị form xóa
$('#modalXoaSanPham').on('show.bs.modal', function (event) {
    let button = $(event.relatedTarget);
    let id = button.data('id');
    let chuoiGiaoDien = `<div>Bạn có chắc chắc muốn xóa sản phẩm ${$(`#${id}`).find('td')[1].innerText}?</div>`
    $(this).find('.modal-body').html(chuoiGiaoDien);
    //them data-id vào modal footer để nút xác nhận xóa có thể lấy id
    $(this).find('.modal-footer').attr('data-id', id);
})
//xác nhận xóa
$('#btnXacNhanXoa').click(function () {
    //bug timing??
    let idSanPhamXoa = $(this).parent().data('id');
    console.log(idSanPhamXoa);
    $.get(`/admin/san-pham/xoa/${idSanPhamXoa}`, (data, textStatus) => {
        if (data == "true") {
            $('#modalXoaSanPham').modal('hide');
            alert('Xóa thành công');
            //cập nhật lại DOM chỗ table
            $(`tbody #${idSanPhamXoa}`).remove();
        } else {
            $('#modalXoaSanPham').modal('hide');
            alert('Xóa thất bại')
        }
    })
})