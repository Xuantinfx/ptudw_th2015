const themNguoiDung = form => {
  let error = form.getElementsByTagName("label")[4];
  error.innerHTML = `<i class="fas fa-spinner fa-spin"></i>`;
  if (form.hoVaTen.value == "") {
    error.innerText = "Không được để trống họ và tên";
    return false;
  }
  if (form.email.value == "") {
    error.innerText = "Không được để trống email";
    return false;
  }
  if (form.matKhau.value == "") {
    error.innerText = "Không được để trống mật khẩu";
    return false;
  }
  if (form.nhaplaimatkhau.value == "") {
    error.innerText = "Không được để trống nhập lại mật khẩu";
    return false;
  }
  if (form.matKhau.value != form.nhaplaimatkhau.value) {
    error.innerText = "Mật khẩu và mật khẩu nhập lại không khớp";
    return false;
  }
  $.post(
    "/admin/nguoi-dung/them-nguoi-dung",
    {
      hoVaTen: form.hoVaTen.value,
      email: form.email.value,
      matKhau: form.matKhau.value
    },
    data => {
      data = JSON.parse(data);
      if (data.status == true) {
        location.reload();
      } else {
        error.innerText = data.message;
      }
    }
  );
  return false;
};
let fo;
const capNhatNguoiDung = form => {
  let error = form.getElementsByTagName("label")[2];
  error.innerHTML = `<i class="fas fa-spinner fa-spin"></i>`;
  if (form.hoVaTen.value == "") {
    error.innerText = "Không được để trống họ và tên";
    return false;
  }
  if (form.email.value == "") {
    error.innerText = "Không được để trống email";
    return false;
  }
  $.post(
    "/admin/nguoi-dung/cap-nhat-nguoi-dung",
    {
      hoVaTen: form.hoVaTen.value,
      email: form.email.value
    },
    data => {
      data = JSON.parse(data);
      if (data.status == true) {
        location.reload();
      } else {
        error.innerText = data.message;
      }
    }
  );
  return false;
};

const resetPassWord = (email, idSpan) => {
  if (email == undefined || email == "") {
    return false;
  }
  let error = document.getElementById(idSpan);
  error.innerHTML = `<i class="fas fa-spinner fa-spin"></i>`;
  $.post(
    "/admin/nguoi-dung/reset-password-nguoi-dung",
    {
      email: email
    },
    data => {
      data = JSON.parse(data);
      if (data.status == true) {
        error.innerText = "đã đổi thành 123456";
      } else {
        error.innerText = data.message;
      }
    }
  );
};

const deleteNguoiDung = (email, idError) => {
  if (email == undefined || email == "") {
    return false;
  }
  let error = document.getElementById(idError);
  error.innerHTML = `<i class="fas fa-spinner fa-spin "></i>`;
  $.post(
    "/admin/nguoi-dung/xoa-nguoi-dung",
    {
      email: email
    },
    data => {
      data = JSON.parse(data);
      if (data.status == true) {
        location.reload();
      } else {
        error.innerText = data.message;
      }
    }
  );
};
